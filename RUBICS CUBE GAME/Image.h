/****************************************************************************
** This file was made to enable loading image textures
** This code is part of Individual Project Software
** Antreas Josephides ID :201026404
*****************************************************************************/
#ifndef _IMAGE_
#define _IMAGE_

#include<string>
#include <QImage>
#include <GL/glu.h>

// For image 1
class Image
{
  public:

  Image(const std::string& fn);

  ~Image();

  const GLubyte* imageField() const;

  unsigned int Width()  const { return _width;}
  unsigned int Height() const {return _height;}

  private:

  Image(const Image&);

  unsigned int _width;
  unsigned int _height;

  QImage* p_qimage;
  GLubyte* _image;
};

// For image 2
class Image2
{
  public:

  Image2(const std::string& fn);

  ~Image2();

  const GLubyte* imageField() const;

  unsigned int Width()  const { return _width;}
  unsigned int Height() const {return _height;}

  private:

  Image2(const Image&);

  unsigned int _width;
  unsigned int _height;

  QImage* p_qimage;
  GLubyte* _image2;
};
// For image 3
class Image3
{
  public:

  Image3(const std::string& fn);

  ~Image3();

  const GLubyte* imageField() const;

  unsigned int Width()  const { return _width;}
  unsigned int Height() const {return _height;}

  private:

  Image3(const Image&);

  unsigned int _width;
  unsigned int _height;

  QImage* p_qimage;
  GLubyte* _image3;
};

// For image 4
class Image4
{
  public:

  Image4(const std::string& fn);

  ~Image4();

  const GLubyte* imageField() const;

  unsigned int Width()  const { return _width;}
  unsigned int Height() const {return _height;}

  private:

  Image4(const Image&);

  unsigned int _width;
  unsigned int _height;

  QImage* p_qimage;
  GLubyte* _image4;
};
// For image 3
class Image5
{
  public:

  Image5(const std::string& fn);

  ~Image5();

  const GLubyte* imageField() const;

  unsigned int Width()  const { return _width;}
  unsigned int Height() const {return _height;}

  private:

  Image5(const Image&);

  unsigned int _width;
  unsigned int _height;

  QImage* p_qimage;
  GLubyte* _image5;
};
// For image 3
class Image6
{
  public:

  Image6(const std::string& fn);

  ~Image6();

  const GLubyte* imageField() const;

  unsigned int Width()  const { return _width;}
  unsigned int Height() const {return _height;}

  private:

  Image6(const Image&);

  unsigned int _width;
  unsigned int _height;

  QImage* p_qimage;
  GLubyte* _image6;
};


#endif
