/****************************************************************************
** This file was made to enable loading image textures
** This code is part of Individual Project Software
** Antreas Josephides ID :201026404
*****************************************************************************/
#include "Image.h"
#include <vector>
#include <iostream>
#include <cstdlib>

// For image 1

Image::Image(const std::string& file_name)
{
  p_qimage = new QImage(QString(file_name.c_str()));
  _width  = p_qimage->width();
  _height = p_qimage->height();
  _image = new GLubyte[_width*_height*3];

  unsigned int nm = _width*_height;
  for (unsigned int i = 0; i < nm; i++)
  {
    std::div_t part = std::div((int)i, (int)_width);
    QRgb colval = p_qimage->pixel(_width-part.rem-1, part.quot);
    _image[3*nm-3*i-3] = qRed(colval);
    _image[3*nm-3*i-2] = qGreen(colval);
    _image[3*nm-3*i-1] = qBlue(colval);
  }
}

const GLubyte* Image::imageField() const
{
  return _image;
}

Image::~Image()
{
  delete _image;
  delete p_qimage;
}
// For image 2

Image2::Image2(const std::string& file_name)
{
  p_qimage = new QImage(QString(file_name.c_str()));

  _width  = p_qimage->width();
  _height = p_qimage->height();

  _image2 = new GLubyte[_width*_height*3];


  unsigned int nm = _width*_height;
  for (unsigned int i = 0; i < nm; i++)
  {
    std::div_t part = std::div((int)i, (int)_width);
    QRgb colval = p_qimage->pixel(_width-part.rem-1, part.quot);
    _image2[3*nm-3*i-3] = qRed(colval);
    _image2[3*nm-3*i-2] = qGreen(colval);
    _image2[3*nm-3*i-1] = qBlue(colval);
  }
}

const GLubyte* Image2::imageField() const
{
  return _image2;
}

Image2::~Image2()
{
  delete _image2;
  delete p_qimage;
}
// For image 3

Image3::Image3(const std::string& file_name)
{
  p_qimage = new QImage(QString(file_name.c_str()));
  _width  = p_qimage->width();
  _height = p_qimage->height();
  _image3 = new GLubyte[_width*_height*3];

  unsigned int nm = _width*_height;
  for (unsigned int i = 0; i < nm; i++)
  {
    std::div_t part = std::div((int)i, (int)_width);
    QRgb colval = p_qimage->pixel(_width-part.rem-1, part.quot);
    _image3[3*nm-3*i-3] = qRed(colval);
    _image3[3*nm-3*i-2] = qGreen(colval);
    _image3[3*nm-3*i-1] = qBlue(colval);
  }
}

const GLubyte* Image3::imageField() const
{
  return _image3;
}

Image3::~Image3()
{
  delete _image3;
  delete p_qimage;
}

// For image 4

Image4::Image4(const std::string& file_name)
{
  p_qimage = new QImage(QString(file_name.c_str()));
  _width  = p_qimage->width();
  _height = p_qimage->height();
  _image4 = new GLubyte[_width*_height*3];

  unsigned int nm = _width*_height;
  for (unsigned int i = 0; i < nm; i++)
  {
    std::div_t part = std::div((int)i, (int)_width);
    QRgb colval = p_qimage->pixel(_width-part.rem-1, part.quot);
    _image4[3*nm-3*i-3] = qRed(colval);
    _image4[3*nm-3*i-2] = qGreen(colval);
    _image4[3*nm-3*i-1] = qBlue(colval);
  }
}

const GLubyte* Image4::imageField() const
{
  return _image4;
}

Image4::~Image4()
{
  delete _image4;
  delete p_qimage;
}

// For image 5

Image5::Image5(const std::string& file_name)
{
  p_qimage = new QImage(QString(file_name.c_str()));
  _width  = p_qimage->width();
  _height = p_qimage->height();
  _image5 = new GLubyte[_width*_height*3];

  unsigned int nm = _width*_height;
  for (unsigned int i = 0; i < nm; i++)
  {
    std::div_t part = std::div((int)i, (int)_width);
    QRgb colval = p_qimage->pixel(_width-part.rem-1, part.quot);
    _image5[3*nm-3*i-3] = qRed(colval);
    _image5[3*nm-3*i-2] = qGreen(colval);
    _image5[3*nm-3*i-1] = qBlue(colval);
  }
}

const GLubyte* Image5::imageField() const
{
  return _image5;
}

Image5::~Image5()
{
  delete _image5;
  delete p_qimage;
}

// For image 6

Image6::Image6(const std::string& file_name)
{
  p_qimage = new QImage(QString(file_name.c_str()));
  _width  = p_qimage->width();
  _height = p_qimage->height();
  _image6 = new GLubyte[_width*_height*3];

  unsigned int nm = _width*_height;
  for (unsigned int i = 0; i < nm; i++)
  {
    std::div_t part = std::div((int)i, (int)_width);
    QRgb colval = p_qimage->pixel(_width-part.rem-1, part.quot);
    _image6[3*nm-3*i-3] = qRed(colval);
    _image6[3*nm-3*i-2] = qGreen(colval);
    _image6[3*nm-3*i-1] = qBlue(colval);
  }
}

const GLubyte* Image6::imageField() const
{
  return _image6;
}

Image6::~Image6()
{
  delete _image6;
  delete p_qimage;
}
