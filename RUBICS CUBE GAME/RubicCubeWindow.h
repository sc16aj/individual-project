/****************************************************************************
** This file incluse coding for executing the features of the software explained below
** This code is part of Individual Project Software
** Antreas Josephides ID :201026404
*****************************************************************************/
#ifndef __GL_POLYGON_WINDOW_H__
#define __GL_POLYGON_WINDOW_H__ 1

#include <QGLWidget>
#include <QMenuBar>
#include <QSlider>
#include <QBoxLayout>
#include <QTimer>
#include "RubicWidget.h"

class RubicCubeWindow: public QWidget
{
	public:


	RubicCubeWindow(QWidget *parent); // constructor / destructor
	~RubicCubeWindow();

	QMenuBar *menuBar; 	// menu bar

		QMenu *fileMenu; 		// file menu

			QAction *actionQuit; // quit action

	QBoxLayout *windowLayout; 	// window layout

	RubicWidget *cubeWidget; 	// Main Widget

	QSlider *nSlider; // Slider

	QSlider *nSlider2; // Slider 2

	QTimer* ptimer; 	// timer for animation

		QTimer* ptimer2; 	// timer for animation

	void ResetInterface(); // resets interface elements
};

#endif
