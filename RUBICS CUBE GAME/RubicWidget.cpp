/****************************************************************************
** This file where most of the code is.
** This code is part of Individual Project Software
*****************************************************************************/


//LIBRARIES INCLUDES
#include <GL/glu.h>
#include <QGLWidget>
#include <GL/glut.h>
#include <QSlider>
#include <cmath>
#include <iostream>
#include "RubicWidget.h"
#include <stdio.h>


// ALL SMALL CUBES CLASSIFIED NUMBER AND INITIALS FOR COLOURS
// COLOURS:  GREEN(G)   RED(R)   BLUE(B)   ORANGE(O)   WHITE(W)   YELLOW(Y)
//                    ┏━━━┳━━━┳━━━┳━━━┳━━━┳━━━┳━━━┳━━━┳━━━┓
//                    ┃1x1┃2x1┃3x1┃4x1┃5x1┃6x1┃7x1┃8x1┃9x1┃
//                    ┣━━━╋━━━╋━━━╋━━━╋━━━╋━━━╋━━━╋━━━╋━━━┫
//                    ┃GOW┃OW ┃BOW┃GW ┃ W ┃BW ┃GRW┃RW ┃RBW┃
//                    ┣━━━╋━━━╋━━━╋━━━╋━━━╋━━━╋━━━╋━━━╋━━━┫
//                    ┃1x2┃2x2┃3x2┃4x2┃5x2┃6x2┃7x2┃8x2┃9x2┃
//                    ┣━━━╋━━━╋━━━╋━━━╋━━━╋━━━╋━━━╋━━━╋━━━┫
//                    ┃GO ┃ O ┃BO ┃ G ┃╳╳╳┃ B ┃GR ┃ R ┃RB ┃
//                    ┣━━━╋━━━╋━━━╋━━━╋━━━╋━━━╋━━━╋━━━╋━━━┫
//                    ┃1x3┃2x3┃3x3┃4x3┃5x3┃6x3┃7x3┃8x3┃9x3┃
//                    ┣━━━╋━━━╋━━━╋━━━╋━━━╋━━━╋━━━╋━━━╋━━━┫
//                    ┃GOY┃OY ┃BOY┃GY ┃ Y ┃BY ┃GRY┃RY ┃RBY┃
//                    ┗━━━┻━━━┻━━━┻━━━┻━━━┻━━━┻━━━┻━━━┻━━━┛

// PARAMETERS THAT DEFINE THE AI PLAYErS DIFICULTIES
int Oliver = 2;
int Karoline = 4;
int Benjamin = 6;



//CURRENT OPPONENT
int Opponent = Oliver;


float A, B, C, D, E, F, G, H, I, J, K, L = 0;
float Start =0;
int EyesOn = 0;
int MixMoveA = 0;
int MixMoveB = 0;
int MixMoveC = 0;
int MixMoveD = 0;
int MixMoveE = 0;
int MixMoves = 0;
int SolveMoves =0;

int CrossMoveA, CrossMoveB, CrossMoveC, CrossMoveD, CrossMoveE, CrossMoveF, CrossMoveG, CrossMoveH = 0;

int TestingVar = 0;
int SOLVED = 0;
float Timervar = 0;

float Angle1x1, Angle2x1, Angle3x1, Angle4x1, Angle5x1, Angle6x1, Angle7x1, Angle8x1, Angle9x1;
float Angle1x2, Angle2x2, Angle3x2, Angle4x2, Angle5x2, Angle6x2, Angle7x2, Angle8x2, Angle9x2;
float Angle1x3, Angle2x3, Angle3x3, Angle4x3, Angle5x3, Angle6x3, Angle7x3, Angle8x3, Angle9x3;



int POSX1x1, POSX4x1, POSX7x1, POSX1x2, POSX4x2, POSX7x2, POSX1x3, POSX4x3, POSX7x3;
int POSX2x1, POSX5x1, POSX8x1, POSX2x2, POSX5x2, POSX8x2, POSX2x3, POSX5x3, POSX8x3;
int POSX3x1, POSX6x1, POSX9x1, POSX3x2, POSX6x2, POSX9x2, POSX3x3, POSX6x3, POSX9x3;

int POSY1x1, POSY2x1, POSY3x1, POSY1x2, POSY2x2, POSY3x2, POSY1x3, POSY2x3, POSY3x3 ;
int POSY4x1, POSY5x1, POSY6x1, POSY4x2, POSY5x2, POSY6x2, POSY4x3, POSY5x3, POSY6x3 ;
int POSY7x1, POSY8x1, POSY9x1, POSY7x2, POSY8x2, POSY9x2, POSY7x3, POSY8x3, POSY9x3 ;

int POSZ1x1, POSZ2x1, POSZ3x1, POSZ4x1, POSZ5x1, POSZ6x1, POSZ7x1, POSZ8x1, POSZ9x1 ;
int POSZ1x2, POSZ2x2, POSZ3x2, POSZ4x2, POSZ5x2, POSZ6x2, POSZ7x2, POSZ8x2, POSZ9x2 ;
int POSZ1x3, POSZ2x3, POSZ3x3, POSZ4x3, POSZ5x3, POSZ6x3, POSZ7x3, POSZ8x3, POSZ9x3 ;

int POS2X1x1, POS2X4x1, POS2X7x1, POS2X1x2, POS2X4x2, POS2X7x2, POS2X1x3, POS2X4x3, POS2X7x3;
int POS2X2x1, POS2X5x1, POS2X8x1, POS2X2x2, POS2X5x2, POS2X8x2, POS2X2x3, POS2X5x3, POS2X8x3;
int POS2X3x1, POS2X6x1, POS2X9x1, POS2X3x2, POS2X6x2, POS2X9x2, POS2X3x3, POS2X6x3, POS2X9x3;

int POS2Y1x1, POS2Y2x1, POS2Y3x1, POS2Y1x2, POS2Y2x2, POS2Y3x2, POS2Y1x3, POS2Y2x3, POS2Y3x3 ;
int POS2Y4x1, POS2Y5x1, POS2Y6x1, POS2Y4x2, POS2Y5x2, POS2Y6x2, POS2Y4x3, POS2Y5x3, POS2Y6x3 ;
int POS2Y7x1, POS2Y8x1, POS2Y9x1, POS2Y7x2, POS2Y8x2, POS2Y9x2, POS2Y7x3, POS2Y8x3, POS2Y9x3 ;

int POS2Z1x1, POS2Z2x1, POS2Z3x1, POS2Z4x1, POS2Z5x1, POS2Z6x1, POS2Z7x1, POS2Z8x1, POS2Z9x1 ;
int POS2Z1x2, POS2Z2x2, POS2Z3x2, POS2Z4x2, POS2Z5x2, POS2Z6x2, POS2Z7x2, POS2Z8x2, POS2Z9x2 ;
int POS2Z1x3, POS2Z2x3, POS2Z3x3, POS2Z4x3, POS2Z5x3, POS2Z6x3, POS2Z7x3, POS2Z8x3, POS2Z9x3 ;

int LOCX1x1= -2.2, LOCX4x1= -2.2, LOCX7x1= -2.2, LOCX1x2= -2.2, LOCX4x2= -2.2, LOCX7x2= -2.2, LOCX1x3= -2.2, LOCX4x3= -2.2, LOCX7x3= -2.2;
int LOCX2x1= 0, LOCX5x1= 0, LOCX8x1= 0, LOCX2x2= 0, LOCX5x2= 0, LOCX8x2= 0, LOCX2x3= 0, LOCX5x3= 0, LOCX8x3= 0;
int LOCX3x1= 2.2, LOCX6x1= 2.2, LOCX9x1= 2.2, LOCX3x2= 2.2, LOCX6x2= 2.2, LOCX9x2= 2.2, LOCX3x3= 2.2, LOCX6x3= 2.2, LOCX9x3= 2.2;

int LOCY1x1= -2.2, LOCY2x1= -2.2, LOCY3x1= -2.2, LOCY1x2= -2.2, LOCY2x2= -2.2, LOCY3x2= -2.2, LOCY1x3= -2.2, LOCY2x3= -2.2, LOCY3x3 =-2.2;
int LOCY4x1=0, LOCY5x1=0, LOCY6x1=0, LOCY4x2=0, LOCY5x2=0, LOCY6x2=0, LOCY4x3=0, LOCY5x3=0, LOCY6x3 =0;
int LOCY7x1=2.2, LOCY8x1=2.2, LOCY9x1=2.2, LOCY7x2=2.2, LOCY8x2=2.2, LOCY9x2=2.2, LOCY7x3=2.2, LOCY8x3=2.2, LOCY9x3 =2.2;

int LOCZ1x1=-2.2, LOCZ2x1=-2.2, LOCZ3x1=-2.2, LOCZ4x1=-2.2, LOCZ5x1=-2.2, LOCZ6x1=-2.2, LOCZ7x1=-2.2, LOCZ8x1=-2.2, LOCZ9x1 =-2.2;
int LOCZ1x2=0, LOCZ2x2=0, LOCZ3x2=0, LOCZ4x2=0, LOCZ5x2=0, LOCZ6x2=0, LOCZ7x2=0, LOCZ8x2=0, LOCZ9x2 =0;
int LOCZ1x3=2.2, LOCZ2x3=2.2, LOCZ3x3=2.2, LOCZ4x3=2.2, LOCZ5x3=2.2, LOCZ6x3=2.2, LOCZ7x3=2.2, LOCZ8x3=2.2, LOCZ9x3 =2.2;

int LOC2X1x1= -2.2, LOC2X4x1= -2.2, LOC2X7x1= -2.2, LOC2X1x2= -2.2, LOC2X4x2= -2.2, LOC2X7x2= -2.2, LOC2X1x3= -2.2, LOC2X4x3= -2.2, LOC2X7x3= -2.2;
int LOC2X2x1= 0, LOC2X5x1= 0, LOC2X8x1= 0, LOC2X2x2= 0, LOC2X5x2= 0, LOC2X8x2= 0, LOC2X2x3= 0, LOC2X5x3= 0, LOC2X8x3= 0;
int LOC2X3x1= 2.2, LOC2X6x1= 2.2, LOC2X9x1= 2.2, LOC2X3x2= 2.2, LOC2X6x2= 2.2, LOC2X9x2= 2.2, LOC2X3x3= 2.2, LOC2X6x3= 2.2, LOC2X9x3= 2.2;

int LOC2Y1x1= -2.2, LOC2Y2x1= -2.2, LOC2Y3x1= -2.2, LOC2Y1x2= -2.2, LOC2Y2x2= -2.2, LOC2Y3x2= -2.2, LOC2Y1x3= -2.2, LOC2Y2x3= -2.2, LOC2Y3x3 =-2.2;
int LOC2Y4x1=0, LOC2Y5x1=0, LOC2Y6x1=0, LOC2Y4x2=0, LOC2Y5x2=0, LOC2Y6x2=0, LOC2Y4x3=0, LOC2Y5x3=0, LOC2Y6x3 =0;
int LOC2Y7x1=2.2, LOC2Y8x1=2.2, LOC2Y9x1=2.2, LOC2Y7x2=2.2, LOC2Y8x2=2.2, LOC2Y9x2=2.2, LOC2Y7x3=2.2, LOC2Y8x3=2.2, LOC2Y9x3 =2.2;

int LOC2Z1x1=-2.2, LOC2Z2x1=-2.2, LOC2Z3x1=-2.2, LOC2Z4x1=-2.2, LOC2Z5x1=-2.2, LOC2Z6x1=-2.2, LOC2Z7x1=-2.2, LOC2Z8x1=-2.2, LOC2Z9x1 =-2.2;
int LOC2Z1x2=0, LOC2Z2x2=0, LOC2Z3x2=0, LOC2Z4x2=0, LOC2Z5x2=0, LOC2Z6x2=0, LOC2Z7x2=0, LOC2Z8x2=0, LOC2Z9x2 =0;
int LOC2Z1x3= 2.2, LOC2Z2x3=2.2, LOC2Z3x3=2.2, LOC2Z4x3=2.2, LOC2Z5x3=2.2, LOC2Z6x3=2.2, LOC2Z7x3=2.2, LOC2Z8x3=2.2, LOC2Z9x3 =2.2;

int RX1x1, RX2x1, RX3x1, RX4x1, RX5x1, RX6x1, RX7x1, RX8x1, RX9x1;
int RX1x2, RX2x2, RX3x2, RX4x2, RX5x2, RX6x2, RX7x2, RX8x2, RX9x2;
int RX1x3, RX2x3, RX3x3, RX4x3, RX5x3, RX6x3, RX7x3, RX8x3, RX9x3;

int RY1x1, RY2x1, RY3x1, RY4x1, RY5x1, RY6x1, RY7x1, RY8x1, RY9x1;
int RY1x2, RY2x2, RY3x2, RY4x2, RY5x2, RY6x2, RY7x2, RY8x2, RY9x2;
int RY1x3, RY2x3, RY3x3, RY4x3, RY5x3, RY6x3, RY7x3, RY8x3, RY9x3;

int RZ1x1, RZ2x1, RZ3x1, RZ4x1, RZ5x1, RZ6x1, RZ7x1, RZ8x1, RZ9x1;
int RZ1x2, RZ2x2, RZ3x2, RZ4x2, RZ5x2, RZ6x2, RZ7x2, RZ8x2, RZ9x2;
int RZ1x3, RZ2x3, RZ3x3, RZ4x3, RZ5x3, RZ6x3, RZ7x3, RZ8x3, RZ9x3;





float SPIN, SPIN2, SPIN3 = 0;


typedef struct materialStruct // Material properties
  {
    GLfloat ambient[4];
    GLfloat diffuse[4];
    GLfloat specular[4];
    GLfloat shininess;
  } materialStruct;

static materialStruct polishedSilver = // Polished Silver Color
  {
    { 0.23125f, 0.23125f, 0.23125f, 1.0f },
    {0.2775f, 0.2775f, 0.2775f, 1.0f },
    {0.773911f, 0.773911f, 0.773911f, 1.0f },
    89.6f
  };
static materialStruct Emerald = // Emerald Color
  {
    {  0.0215f, 0.1745f, 0.0215f, 0.55f },
    {0.07568f, 0.61424f, 0.07568f, 0.55f},
    {0.633f, 0.727811f, 0.633f, 0.55f},
    76.0f
  };


RubicWidget::RubicWidget(QWidget *parent) // Constructor
  : QGLWidget(parent),
    Move1(0),
    Xrotation(0),                            // object movement
    Yrotation(0),              // Interaction movement
    _image("./Textures/pacman.jpg"),         // Image 1
    _image2("./INSTRUCTIONS/MIXES.jpg"),       // Image 2
    _image3("./INSTRUCTIONS/SOLVES.jpg"),       // Image 3
    _image4("./INSTRUCTIONS/LOST.jpg"),      // Image 4
    _image5("./INSTRUCTIONS/"),       // Image 5
    _image6("./INSTRUCTIONS/")       // Image 6
	{                                          // Quadric Objects
    pCylinderX = gluNewQuadric();
    pCylinderY = gluNewQuadric();
    pCylinderZ = gluNewQuadric();
	}

void RubicWidget::initializeGL() // called when OpenGL context is set upYaxisRotation
	{
    glClearColor(1, 0.3, 0.8, 0.0); // set the widget background colour

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);


 	}



RubicWidget::~RubicWidget() // Delete Quadric Objects
{
  gluDeleteQuadric(pCylinderX);
  gluDeleteQuadric(pCylinderY);
  gluDeleteQuadric(pCylinderZ);
}

void RubicWidget::updateXrotation(int i) // Object movement
{
  Xrotation = i;
  this->repaint();
}
void RubicWidget::updateYrotation(int i) // Interaction movement
{
  Yrotation  = i;
  this->repaint();
}
void RubicWidget::updateMethod1Moves() // Interaction movement
{
//INSTRUCTIONS TO SOLVE CUBE WITH METHOD 1

  if (MixMoveA !=1)
    {
      Start +=5;
      this->repaint();
    }
  if (Start == 95)
    {
      Start = 90;
      MixMoveA = 1;
    }

  if (MixMoveB != 1)
    {
      A +=5;
      this->repaint();
    }

  if (A == 185)
    {
      A = 180;
      MixMoveB = 1;
    }

  if (MixMoveC != 1)
    {
      B +=5;
      this->repaint();
    }

  if (B == 275)
    {
      B = 270;
      MixMoveC = 1;
    }

  if (MixMoveD != 1)
    {
      C +=5;
      this->repaint();
    }

  if (C == 365)
    {
      C = 360;
      MixMoveD = 1;
    }

  if (CrossMoveA != 1)
    {
      D +=5;
      this->repaint();
    }

  if (D == 455)
    {
      D= 450;
      CrossMoveA = 1;
    }

  if (CrossMoveB != 1)
    {
      E +=5;
      this->repaint();
    }

  if (E == 545)
    {
      E = 540;
      CrossMoveB = 1;
    }

  if (CrossMoveC != 1)
    {
      F +=5;
      this->repaint();
    }

  if (F == 635)
    {
      F = 630;
      CrossMoveC = 1;
    }

  if (CrossMoveD != 1)
    {
      G +=5;
      this->repaint();
    }

  if (G == 725)
    {
      G = 720;
      CrossMoveD = 1;
    }

  if (CrossMoveE != 1)
    {
      H+=5;
      this->repaint();
    }

  if (H == 815)
    {
      H = 810;
      CrossMoveE = 1;
      SPIN =1;
    }

  if (CrossMoveF != 1)
    {
      I+=5;
      this->repaint();
    }

  if (I == 905)
    {
      I = 900;
      CrossMoveF = 1;
    }

  if (CrossMoveG != 1)
    {
      J+=5;
      this->repaint();
    }

  if (J == 995)
    {
      J = 990;
      CrossMoveG = 1;
    }

  if (CrossMoveH != 1)
    {
      K+=5;
      this->repaint();
    }

  if (K == 1085)
    {
      K = 1080;
      CrossMoveH = 1;
    }


}



//INSTRUCTIONS TO SOLVE CUBE WITH METHOD 2 ( AI )
void RubicWidget::updateMethod2Moves()
{

// THE CUBE IS MIXED WITH THE FOLLOWING MOVES:
//
/*
Timervar +=1;
if (Timervar >= 100)
{
  LOCX1x1= -2.2, LOCX4x1= -2.2, LOCX7x1= -2.2, LOCX1x2= -2.2, LOCX4x2= -2.2, LOCX7x2= -2.2, LOCX1x3= -2.2, LOCX4x3= -2.2, LOCX7x3= -2.2;
  LOCX2x1= 0, LOCX5x1= 0, LOCX8x1= 0, LOCX2x2= 0, LOCX5x2= 0, LOCX8x2= 0, LOCX2x3= 0, LOCX5x3= 0, LOCX8x3= 0;
  LOCX3x1= 2.2, LOCX6x1= 2.2, LOCX9x1= 2.2, LOCX3x2= 2.2, LOCX6x2= 2.2, LOCX9x2= 2.2, LOCX3x3= 2.2, LOCX6x3= 2.2, LOCX9x3= 2.2;

  LOCY1x1= -2.2, LOCY2x1= -2.2, LOCY3x1= -2.2, LOCY1x2= -2.2, LOCY2x2= -2.2, LOCY3x2= -2.2, LOCY1x3= -2.2, LOCY2x3= -2.2, LOCY3x3 =-2.2;
  LOCY4x1=0, LOCY5x1=0, LOCY6x1=0, LOCY4x2=0, LOCY5x2=0, LOCY6x2=0, LOCY4x3=0, LOCY5x3=0, LOCY6x3 =0;
  LOCY7x1=2.2, LOCY8x1=2.2, LOCY9x1=2.2, LOCY7x2=2.2, LOCY8x2=2.2, LOCY9x2=2.2, LOCY7x3=2.2, LOCY8x3=2.2, LOCY9x3 =2.2;

  LOCZ1x1=-2.2, LOCZ2x1=-2.2, LOCZ3x1=-2.2, LOCZ4x1=-2.2, LOCZ5x1=-2.2, LOCZ6x1=-2.2, LOCZ7x1=-2.2, LOCZ8x1=-2.2, LOCZ9x1 =-2.2;
  LOCZ1x2=0, LOCZ2x2=0, LOCZ3x2=0, LOCZ4x2=0, LOCZ5x2=0, LOCZ6x2=0, LOCZ7x2=0, LOCZ8x2=0, LOCZ9x2 =0;
  LOCZ1x3=2.2; LOCZ2x3=2.2, LOCZ3x3=2.2, LOCZ4x3=2.2, LOCZ5x3=2.2, LOCZ6x3=2.2, LOCZ7x3=2.2, LOCZ8x3=2.2, LOCZ9x3 =2.2;

  LOC2X1x1= -2.2, LOC2X4x1= -2.2, LOC2X7x1= -2.2, LOC2X1x2= -2.2, LOC2X4x2= -2.2, LOC2X7x2= -2.2, LOC2X1x3= -2.2, LOC2X4x3= -2.2, LOC2X7x3= -2.2;
  LOC2X2x1= 0, LOC2X5x1= 0, LOC2X8x1= 0, LOC2X2x2= 0, LOC2X5x2= 0, LOC2X8x2= 0, LOC2X2x3= 0, LOC2X5x3= 0, LOC2X8x3= 0;
  LOC2X3x1= 2.2, LOC2X6x1= 2.2, LOC2X9x1= 2.2, LOC2X3x2= 2.2, LOC2X6x2= 2.2, LOC2X9x2= 2.2, LOC2X3x3= 2.2, LOC2X6x3= 2.2, LOC2X9x3= 2.2;

  LOC2Y1x1= -2.2, LOC2Y2x1= -2.2, LOC2Y3x1= -2.2, LOC2Y1x2= -2.2, LOC2Y2x2= -2.2, LOC2Y3x2= -2.2, LOC2Y1x3= -2.2, LOC2Y2x3= -2.2, LOC2Y3x3 =-2.2;
  LOC2Y4x1=0, LOC2Y5x1=0, LOC2Y6x1=0, LOC2Y4x2=0, LOC2Y5x2=0, LOC2Y6x2=0, LOC2Y4x3=0, LOC2Y5x3=0, LOC2Y6x3 =0;
  LOC2Y7x1=2.2, LOC2Y8x1=2.2, LOC2Y9x1=2.2, LOC2Y7x2=2.2, LOC2Y8x2=2.2, LOC2Y9x2=2.2, LOC2Y7x3=2.2, LOC2Y8x3=2.2, LOC2Y9x3 =2.2;

  LOC2Z1x1=-2.2, LOC2Z2x1=-2.2, LOC2Z3x1=-2.2, LOC2Z4x1=-2.2, LOC2Z5x1=-2.2, LOC2Z6x1=-2.2, LOC2Z7x1=-2.2, LOC2Z8x1=-2.2, LOC2Z9x1 =-2.2;
  LOC2Z1x2=0, LOC2Z2x2=0, LOC2Z3x2=0, LOC2Z4x2=0, LOC2Z5x2=0, LOC2Z6x2=0, LOC2Z7x2=0, LOC2Z8x2=0, LOC2Z9x2 =0;
  LOC2Z1x3= 2.2, LOC2Z2x3=2.2, LOC2Z3x3=2.2, LOC2Z4x3=2.2, LOC2Z5x3=2.2, LOC2Z6x3=2.2, LOC2Z7x3=2.2, LOC2Z8x3=2.2, LOC2Z9x3 =2.2;


MixMoves =1;

  // X MOVES
    if (LOCX1x1 == -2.2)
    {}
    if (LOCX2x1 == -2.2)
    {}
    if (LOCX3x1 == -2.2)
    {}
    if (LOCX4x1 == -2.2)
    {}
    if (LOCX5x1 == -2.2)
    {}
    if (LOCX6x1 == -2.2)
    {}
    if (LOCX7x1 == -2.2)
    {}
    if (LOCX8x1 == -2.2)
    {}
    if (LOCX9x1 == -2.2)
    {}
    if (LOCX1x2 == -2.2)
    {}
    if (LOCX2x2 == -2.2)
    {}
    if (LOCX3x2 == -2.2)
    {}
    if (LOCX4x2 == -2.2)
    {}
    if (LOCX5x2 == -2.2)
    {}
    if (LOCX6x2 == -2.2)
    {}
    if (LOCX7x2 == -2.2)
    {}
    if (LOCX8x2 == -2.2)
    {}
    if (LOCX9x2 == -2.2)
    {}
    if (LOCX1x3 == -2.2)
    {}
    if (LOCX2x3 == -2.2)
    {}
    if (LOCX3x3 == -2.2)
    {}
    if (LOCX4x3 == -2.2)
    {}
    if (LOCX5x3 == -2.2)
    {}
    if (LOCX6x3 == -2.2)
    {}
    if (LOCX7x3 == -2.2)
    {}
    if (LOCX8x3 == -2.2)
    {}
    if (LOCX9x3 == -2.2)
    {}

    if (LOCX1x1 == 0)
    {}
    if (LOCX2x1 == 0)
    {}
    if (LOCX3x1 == 0)
    {}
    if (LOCX4x1 == 0)
    {}
    if (LOCX5x1 == 0)
    {}
    if (LOCX6x1 == 0)
    {}
    if (LOCX7x1 == 0)
    {}
    if (LOCX8x1 == 0)
    {}
    if (LOCX9x1 == 0)
    {}
    if (LOCX1x2 == 0)
    {}
    if (LOCX2x2 == 0)
    {}
    if (LOCX3x2 == 0)
    {}
    if (LOCX4x2 == 0)
    {}
    if (LOCX5x2 == 0)
    {}
    if (LOCX6x2 == 0)
    {}
    if (LOCX7x2 == 0)
    {}
    if (LOCX8x2 == 0)
    {}
    if (LOCX9x2 == 0)
    {}
    if (LOCX1x3 == 0)
    {}
    if (LOCX2x3 == 0)
    {}
    if (LOCX3x3 == 0)
    {}
    if (LOCX4x3 == 0)
    {}
    if (LOCX5x3 == 0)
    {}
    if (LOCX6x3 == 0)
    {}
    if (LOCX7x3 == 0)
    {}
    if (LOCX8x3 == 0)
    {}
    if (LOCX9x3 == 0)
    {}
    if (LOCX1x1 == 2.2)
    {}
    if (LOCX2x1 == 2.2)
    {}
    if (LOCX3x1 == 2.2)
    {}
    if (LOCX4x1 == 2.2)
    {}
    if (LOCX5x1 == 2.2)
    {}
    if (LOCX6x1 == 2.2)
    {}
    if (LOCX7x1 == 2.2)
    {}
    if (LOCX8x1 == 2.2)
    {}
    if (LOCX9x1 == 2.2)
    {}
    if (LOCX1x2 == 2.2)
    {}
    if (LOCX2x2 == 2.2)
    {}
    if (LOCX3x2 == 2.2)
    {}
    if (LOCX4x2 == 2.2)
    {}
    if (LOCX5x2 == 2.2)
    {}
    if (LOCX6x2 == 2.2)
    {}
    if (LOCX7x2 == 2.2)
    {}
    if (LOCX8x2 == 2.2)
    {}
    if (LOCX9x2 == 2.2)
    {}
    if (LOCX1x3 == 2.2)
    {}
    if (LOCX2x3 == 2.2)
    {}
    if (LOCX3x3 == 2.2)
    {}
    if (LOCX4x3 == 2.2)
    {}
    if (LOCX5x3 == 2.2)
    {}
    if (LOCX6x3 == 2.2)
    {}
    if (LOCX7x3 == 2.2)
    {}
    if (LOCX8x3 == 2.2)
    {}
    if (LOCX9x3 == 2.2)
    {}
      // Y MOVES
    if (LOCY1x1 == -2.2)
    {}
    if (LOCY2x1 == -2.2)
    {}
    if (LOCY3x1 == -2.2)
    {}
    if (LOCY4x1 == -2.2)
    {}
    if (LOCY5x1 == -2.2)
    {}
    if (LOCY6x1 == -2.2)
    {}
    if (LOCY7x1 == -2.2)
    {}
    if (LOCY8x1 == -2.2)
    {}
    if (LOCY9x1 == -2.2)
    {}
    if (LOCY1x2 == -2.2)
    {}
    if (LOCY2x2 == -2.2)
    {}
    if (LOCY3x2 == -2.2)
    {}
    if (LOCY4x2 == -2.2)
    {}
    if (LOCY5x2 == -2.2)
    {}
    if (LOCY6x2 == -2.2)
    {}
    if (LOCY7x2 == -2.2)
    {}
    if (LOCY8x2 == -2.2)
    {}
    if (LOCY9x2 == -2.2)
    {}
    if (LOCY1x3 == -2.2)
    {}
    if (LOCY2x3 == -2.2)
    {}
    if (LOCY3x3 == -2.2)
    {}
    if (LOCY4x3 == -2.2)
    {}
    if (LOCY5x3 == -2.2)
    {}
    if (LOCY6x3 == -2.2)
    {}
    if (LOCY7x3 == -2.2)
    {}
    if (LOCY8x3 == -2.2)
    {}
    if (LOCY9x3 == -2.2)
    {}

    if (LOCY1x1 == 0)
    {}
    if (LOCY2x1 == 0)
    {}
    if (LOCY3x1 == 0)
    {}
    if (LOCY4x1 == 0)
    {}
    if (LOCY5x1 == 0)
    {}
    if (LOCY6x1 == 0)
    {}
    if (LOCY7x1 == 0)
    {}
    if (LOCY8x1 == 0)
    {}
    if (LOCY9x1 == 0)
    {}
    if (LOCY1x2 == 0)
    {}
    if (LOCY2x2 == 0)
    {}
    if (LOCY3x2 == 0)
    {}
    if (LOCY4x2 == 0)
    {}
    if (LOCY5x2 == 0)
    {}
    if (LOCY6x2 == 0)
    {}
    if (LOCY7x2 == 0)
    {}
    if (LOCY8x2 == 0)
    {}
    if (LOCY9x2 == 0)
    {}
    if (LOCY1x3 == 0)
    {}
    if (LOCY2x3 == 0)
    {}
    if (LOCY3x3 == 0)
    {}
    if (LOCY4x3 == 0)
    {}
    if (LOCY5x3 == 0)
    {}
    if (LOCY6x3 == 0)
    {}
    if (LOCY7x3 == 0)
    {}
    if (LOCY8x3 == 0)
    {}
    if (LOCY9x3 == 0)
    {}
    if (LOCY1x1 == 2.2)
    {}
    if (LOCY2x1 == 2.2)
    {}
    if (LOCY3x1 == 2.2)
    {}
    if (LOCY4x1 == 2.2)
    {}
    if (LOCY5x1 == 2.2)
    {}
    if (LOCY6x1 == 2.2)
    {}
    if (LOCY7x1 == 2.2)
    {}
    if (LOCY8x1 == 2.2)
    {}
    if (LOCY9x1 == 2.2)
    {}
    if (LOCY1x2 == 2.2)
    {}
    if (LOCY2x2 == 2.2)
    {}
    if (LOCY3x2 == 2.2)
    {}
    if (LOCY4x2 == 2.2)
    {}
    if (LOCY5x2 == 2.2)
    {}
    if (LOCY6x2 == 2.2)
    {}
    if (LOCY7x2 == 2.2)
    {}
    if (LOCY8x2 == 2.2)
    {}
    if (LOCY9x2 == 2.2)
    {}
    if (LOCY1x3 == 2.2)
    {}
    if (LOCY2x3 == 2.2)
    {}
    if (LOCY3x3 == 2.2)
    {}
    if (LOCY4x3 == 2.2)
    {}
    if (LOCY5x3 == 2.2)
    {}
    if (LOCY6x3 == 2.2)
    {}
    if (LOCY7x3 == 2.2)
    {}
    if (LOCY8x3 == 2.2)
    {}
    if (LOCY9x3 == 2.2)
    {}
      // Z MOVES
    if (LOCZ1x1 == -2.2)
    {}
    if (LOCZ2x1 == -2.2)
    {}
    if (LOCZ3x1 == -2.2)
    {}
    if (LOCZ4x1 == -2.2)
    {}
    if (LOCZ5x1 == -2.2)
    {}
    if (LOCZ6x1 == -2.2)
    {}
    if (LOCZ7x1 == -2.2)
    {}
    if (LOCZ8x1 == -2.2)
    {}
    if (LOCZ9x1 == -2.2)
    {}
    if (LOCZ1x2 == -2.2)
    {}
    if (LOCZ2x2 == -2.2)
    {}
    if (LOCZ3x2 == -2.2)
    {}
    if (LOCZ4x2 == -2.2)
    {}
    if (LOCZ5x2 == -2.2)
    {}
    if (LOCZ6x2 == -2.2)
    {}
    if (LOCZ7x2 == -2.2)
    {}
    if (LOCZ8x2 == -2.2)
    {}
    if (LOCZ9x2 == -2.2)
    {}

    if (LOCZ1x3 == -2.2)
    {}
    if (LOCZ2x3 == -2.2)
    {}
    if (LOCZ3x3 == -2.2)
    {}
    if (LOCZ4x3 == -2.2)
    {}
    if (LOCZ5x3 == -2.2)
    {}
    if (LOCZ6x3 == -2.2)
    {}
    if (LOCZ7x3 == -2.2)
    {}
    if (LOCZ8x3 == -2.2)
    {}
    if (LOCZ9x3 == -2.2)
    {}

    if (LOCZ1x1 == 0)
    {}
    if (LOCZ2x1 == 0)
    {}
    if (LOCZ3x1 == 0)
    {}
    if (LOCZ4x1 == 0)
    {}
    if (LOCZ5x1 == 0)
    {}
    if (LOCZ6x1 == 0)
    {}
    if (LOCZ7x1 == 0)
    {}
    if (LOCZ8x1 == 0)
    {}
    if (LOCZ9x1 == 0)
    {}
    if (LOCZ1x2 == 0)
    {}
    if (LOCZ2x2 == 0)
    {}
    if (LOCZ3x2 == 0)
    {}
    if (LOCZ4x2 == 0)
    {}
    if (LOCZ5x2 == 0)
    {}
    if (LOCZ6x2 == 0)
    {}
    if (LOCZ7x2 == 0)
    {}
    if (LOCZ8x2 == 0)
    {}
    if (LOCZ9x2 == 0)
    {}
    if (LOCZ1x3 == 0)
    {}
    if (LOCZ2x3 == 0)
    {}
    if (LOCZ3x3 == 0)
    {}
    if (LOCZ4x3 == 0)
    {}
    if (LOCZ5x3 == 0)
    {}
    if (LOCZ6x3 == 0)
    {}
    if (LOCZ7x3 == 0)
    {}
    if (LOCZ8x3 == 0)
    {}
    if (LOCZ9x3 == 0)
    {}
    if (LOCZ1x1 == 2.2)
    {}
    if (LOCZ2x1 == 2.2)
    {}
    if (LOCZ3x1 == 2.2)
    {}
    if (LOCZ4x1 == 2.2)
    {}
    if (LOCZ5x1 == 2.2)
    {}
    if (LOCZ6x1 == 2.2)
    {}
    if (LOCZ7x1 == 2.2)
    {}
    if (LOCZ8x1 == 2.2)
    {}
    if (LOCZ9x1 == 2.2)
    {}
    if (LOCZ1x2 == 2.2)
    {}
    if (LOCZ2x2 == 2.2)
    {}
    if (LOCZ3x2 == 2.2)
    {}
    if (LOCZ4x2 == 2.2)
    {}
    if (LOCZ5x2 == 2.2)
    {}
    if (LOCZ6x2 == 2.2)
    {}
    if (LOCZ7x2 == 2.2)
    {}
    if (LOCZ8x2 == 2.2)
    {}
    if (LOCZ9x2 == 2.2)
    {}
    if (LOCZ1x3 == 2.2)
    {

    }
    if (LOCZ2x3 == 2.2)
    {

    }
    if (LOCZ3x3 == 2.2)
    {

    }
    if (LOCZ4x3 == 2.2)
    {

    }
    if (LOCZ5x3 == 2.2)
    {

    }
    if (LOCZ6x3 == 2.2)
    {

    }
    if (LOCZ7x3 == 2.2)
    {

    }
    if (LOCZ8x3 == 2.2)
    {

    }
    if (LOCZ9x3 == 2.2)
    {}


  SolveMoves =1;



}


this->repaint();
*/
}


void RubicWidget::resizeGL(int w, int h) // called every time the widget is resized
  {
    glViewport(0, 0, w, h); // set the viewport to the entire widget

  	GLfloat light_pos[] = {1., 0.5, 1., 0.}; // light position

  	        // enables lighting and 2D Textures
    glEnable(GL_LIGHT0);
    glEnable(GL_TEXTURE_2D);

    glEnable(GL_DEPTH_TEST); // enables DEPTH TESTING

  	glMatrixMode(GL_MODELVIEW);
  	glLoadIdentity();
  	glLightfv(GL_LIGHT0, GL_POSITION, light_pos); //LIGHTING CONTROL
    glLightf (GL_LIGHT0, GL_SPOT_CUTOFF,150.);
  	glMatrixMode(GL_PROJECTION);


  	glLoadIdentity();
  	glOrtho(-25.0, 25.0, -25.0, 25.0, -25.0, 25.0); // Area of Animation
  }



//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄RUBICES PIECES CREATED┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┐
//                                                                                |
//                                                                                |
//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄FIRST 9 (BACK)┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄.┄┄┄|
void RubicWidget::CubePiece1x1()//                                                |
{

  glColor3f(1.0, 0.5,0.0); //orange
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();


  glColor3f(0.0,1.0,0.0); // green
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glColor3f(0.0,0.0,0.0); //black
    glBegin(GL_POLYGON);
      glVertex3f( 1.0, -1.0,  1.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glVertex3f( 1.0,  1.0, -1.0);
      glVertex3f( 1.0,  1.0,  1.0);
      glEnd();


  glColor3f(1.0,1.0,1.0); //white
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,  -1.0,  1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,   1.0,  1.0);
    glVertex3f( -1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0,  1.0);
    glEnd();
}
void RubicWidget::CubePiece2x1()//                                                |
{
  glColor3f(1.0, 0.5,0.0); //orange
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();


  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glColor3f(0.0,0.0,0.0); //black
    glBegin(GL_POLYGON);
      glVertex3f( 1.0, -1.0,  1.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glVertex3f( 1.0,  1.0, -1.0);
      glVertex3f( 1.0,  1.0,  1.0);
      glEnd();

  glColor3f(1.0,1.0,1.0); //white
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,  -1.0,  1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,   1.0,  1.0);
    glVertex3f( -1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0,  1.0);
    glEnd();
}
void RubicWidget::CubePiece3x1()//                                                |
{

  glColor3f(1.0, 0.5,0.0); //orange
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();


  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glColor3f(0.0, 0.0,1.0); //blue
    glBegin(GL_POLYGON);
      glVertex3f( 1.0, -1.0,  1.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glVertex3f( 1.0,  1.0, -1.0);
      glVertex3f( 1.0,  1.0,  1.0);
      glEnd();

  glColor3f(1.0,1.0,1.0); //white
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,  -1.0,  1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,   1.0,  1.0);
    glVertex3f( -1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0,  1.0);
    glEnd();
}
void RubicWidget::CubePiece4x1()//                                                |
{

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();


  glColor3f(0.0,1.0,0.0); // green
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glColor3f(0.0,0.0,0.0); //black
    glBegin(GL_POLYGON);
      glVertex3f( 1.0, -1.0,  1.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glVertex3f( 1.0,  1.0, -1.0);
      glVertex3f( 1.0,  1.0,  1.0);
      glEnd();

  glColor3f(1.0,1.0,1.0); //white
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,  -1.0,  1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,   1.0,  1.0);
    glVertex3f( -1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0,  1.0);
    glEnd();
}
void RubicWidget::CubePiece5x1()//                                                |
{

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glColor3f(0.0,0.0,0.0); //black
    glBegin(GL_POLYGON);
      glVertex3f( 1.0, -1.0,  1.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glVertex3f( 1.0,  1.0, -1.0);
      glVertex3f( 1.0,  1.0,  1.0);
      glEnd();

  glEnable(GL_TEXTURE_2D);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _image.Width(), _image.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, _image.imageField());
  glColor3f(1.0,1.0,1.0); //white
  glBegin(GL_POLYGON);
    glTexCoord2f(1.1, 0.0);
    glVertex3f( -1.0,  -1.0,  1.0);
    glTexCoord2f(1.1, 1.1);
    glVertex3f( -1.0,  -1.0, -1.0);
    glTexCoord2f(0.0, 1.1);
    glVertex3f(  1.0,  -1.0, -1.0);
    glTexCoord2f(0.0, 0.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,   1.0,  1.0);
    glVertex3f( -1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0,  1.0);
    glEnd();

}
void RubicWidget::CubePiece6x1()//                                                |
{

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();


  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glColor3f(0.0, 0.0,1.0); //blue
    glBegin(GL_POLYGON);
      glVertex3f( 1.0, -1.0,  1.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glVertex3f( 1.0,  1.0, -1.0);
      glVertex3f( 1.0,  1.0,  1.0);
      glEnd();

  glColor3f(1.0,1.0,1.0); //white
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,  -1.0,  1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,   1.0,  1.0);
    glVertex3f( -1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0,  1.0);
    glEnd();
}
void RubicWidget::CubePiece7x1()//                                                |
{

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.5,0.0,0.0); //red
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();


  glColor3f(0.0,1.0,0.0); // green
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glColor3f(0.0,0.0,0.0); //black
    glBegin(GL_POLYGON);
      glVertex3f( 1.0, -1.0,  1.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glVertex3f( 1.0,  1.0, -1.0);
      glVertex3f( 1.0,  1.0,  1.0);
      glEnd();

  glColor3f(1.0,1.0,1.0); //white
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,  -1.0,  1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,   1.0,  1.0);
    glVertex3f( -1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0,  1.0);
    glEnd();
}
void RubicWidget::CubePiece8x1()//                                                |
{

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.5,0.0,0.0); //red
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();


  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glColor3f(0.0,0.0,0.0); //black
    glBegin(GL_POLYGON);
      glVertex3f( 1.0, -1.0,  1.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glVertex3f( 1.0,  1.0, -1.0);
      glVertex3f( 1.0,  1.0,  1.0);
      glEnd();

  glColor3f(1.0,1.0,1.0); //white
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,  -1.0,  1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,   1.0,  1.0);
    glVertex3f( -1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0,  1.0);
    glEnd();
}
void RubicWidget::CubePiece9x1()//                                                |
{

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.5,0.0,0.0); //red
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();


  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glColor3f(0.0, 0.0,1.0); //blue
    glBegin(GL_POLYGON);
      glVertex3f( 1.0, -1.0,  1.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glVertex3f( 1.0,  1.0, -1.0);
      glVertex3f( 1.0,  1.0,  1.0);
      glEnd();

  glColor3f(1.0,1.0,1.0); //white
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,  -1.0,  1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,   1.0,  1.0);
    glVertex3f( -1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0,  1.0);
    glEnd();
}
//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄SECOND 9 (MIDDLE)┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┤
void RubicWidget::CubePiece1x2()//                                                |
{

  glColor3f(1.0, 0.5,0.0); //orange
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();


  glColor3f(0.0,1.0,0.0); // green
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glColor3f(0.0,0.0,0.0); //black
    glBegin(GL_POLYGON);
      glVertex3f( 1.0, -1.0,  1.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glVertex3f( 1.0,  1.0, -1.0);
      glVertex3f( 1.0,  1.0,  1.0);
      glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,  -1.0,  1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,   1.0,  1.0);
    glVertex3f( -1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0,  1.0);
    glEnd();
}
void RubicWidget::CubePiece2x2()//                                                |
{

  glColor3f(1.0, 0.5,0.0); //orange
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();


  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glColor3f(0.0,0.0,0.0); //black
    glBegin(GL_POLYGON);
      glVertex3f( 1.0, -1.0,  1.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glVertex3f( 1.0,  1.0, -1.0);
      glVertex3f( 1.0,  1.0,  1.0);
      glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,  -1.0,  1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,   1.0,  1.0);
    glVertex3f( -1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0,  1.0);
    glEnd();
}
void RubicWidget::CubePiece3x2()//                                                |
{

  glColor3f(1.0, 0.5,0.0); //orange
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();


  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glColor3f(0.0, 0.0,1.0); //blue
    glBegin(GL_POLYGON);
      glVertex3f( 1.0, -1.0,  1.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glVertex3f( 1.0,  1.0, -1.0);
      glVertex3f( 1.0,  1.0,  1.0);
      glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,  -1.0,  1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,   1.0,  1.0);
    glVertex3f( -1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0,  1.0);
    glEnd();
}
void RubicWidget::CubePiece4x2()//                                                |
{

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();


  glColor3f(0.0,1.0,0.0); // green
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glColor3f(0.0,0.0,0.0); //black
    glBegin(GL_POLYGON);
      glVertex3f( 1.0, -1.0,  1.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glVertex3f( 1.0,  1.0, -1.0);
      glVertex3f( 1.0,  1.0,  1.0);
      glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,  -1.0,  1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,   1.0,  1.0);
    glVertex3f( -1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0,  1.0);
    glEnd();
}
void RubicWidget::CubePiece5x2()//                                                |
{

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();


  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glColor3f(0.0,0.0,0.0); //black
    glBegin(GL_POLYGON);
      glVertex3f( 1.0, -1.0,  1.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glVertex3f( 1.0,  1.0, -1.0);
      glVertex3f( 1.0,  1.0,  1.0);
      glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,  -1.0,  1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,   1.0,  1.0);
    glVertex3f( -1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0,  1.0);
    glEnd();
}
void RubicWidget::CubePiece6x2()//                                                |
{

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();


  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glColor3f(0.0, 0.0,1.0); //blue
    glBegin(GL_POLYGON);
      glVertex3f( 1.0, -1.0,  1.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glVertex3f( 1.0,  1.0, -1.0);
      glVertex3f( 1.0,  1.0,  1.0);
      glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,  -1.0,  1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,   1.0,  1.0);
    glVertex3f( -1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0,  1.0);
    glEnd();
}
void RubicWidget::CubePiece7x2()//                                                |
{

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.5,0.0,0.0); //red
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();


  glColor3f(0.0,1.0,0.0); // green
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glColor3f(0.0,0.0,0.0); //black
    glBegin(GL_POLYGON);
      glVertex3f( 1.0, -1.0,  1.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glVertex3f( 1.0,  1.0, -1.0);
      glVertex3f( 1.0,  1.0,  1.0);
      glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,  -1.0,  1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,   1.0,  1.0);
    glVertex3f( -1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0,  1.0);
    glEnd();
}
void RubicWidget::CubePiece8x2()//                                                |
{

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.5,0.0,0.0); //red
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();


  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glColor3f(0.0,0.0,0.0); //black
    glBegin(GL_POLYGON);
      glVertex3f( 1.0, -1.0,  1.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glVertex3f( 1.0,  1.0, -1.0);
      glVertex3f( 1.0,  1.0,  1.0);
      glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,  -1.0,  1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,   1.0,  1.0);
    glVertex3f( -1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0,  1.0);
    glEnd();
}
void RubicWidget::CubePiece9x2()//                                                |
{

    glColor3f(0.0,0.0,0.0); //black
    glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
    glEnd();

    glColor3f(0.5,0.0,0.0); //red
    glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
    glEnd();


    glColor3f(0.0,0.0,0.0); //black
    glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glColor3f(0.0, 0.0,1.0); //blue
    glBegin(GL_POLYGON);
    glVertex3f( 1.0, -1.0,  1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f( 1.0,  1.0,  1.0);
    glEnd();

    glColor3f(0.0,0.0,0.0); //black
    glBegin(GL_POLYGON);
    glVertex3f( -1.0,  -1.0,  1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glEnd();

    glColor3f(0.0,0.0,0.0); //black
    glBegin(GL_POLYGON);
    glVertex3f( -1.0,   1.0,  1.0);
    glVertex3f( -1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0,  1.0);
    glEnd();
}
//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄THIRD 9 (FRONT)┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┤
void RubicWidget::CubePiece1x3()//                                                |
{

  glColor3f(1.0, 0.5,0.0); //orange
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();


  glColor3f(0.0,1.0,0.0); // green
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glColor3f(0.0,0.0,0.0); //black
    glBegin(GL_POLYGON);
      glVertex3f( 1.0, -1.0,  1.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glVertex3f( 1.0,  1.0, -1.0);
      glVertex3f( 1.0,  1.0,  1.0);
      glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,  -1.0,  1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glEnd();

  glColor3f(1.0, 1.0,0.0); //yellow
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,   1.0,  1.0);
    glVertex3f( -1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0,  1.0);
    glEnd();
}
void RubicWidget::CubePiece2x3()//                                                |
{

  glColor3f(1.0, 0.5,0.0); //orange
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();


  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glColor3f(0.0,0.0,0.0); //black
    glBegin(GL_POLYGON);
      glVertex3f( 1.0, -1.0,  1.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glVertex3f( 1.0,  1.0, -1.0);
      glVertex3f( 1.0,  1.0,  1.0);
      glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,  -1.0,  1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glEnd();

  glColor3f(1.0, 1.0,0.0); //yellow
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,   1.0,  1.0);
    glVertex3f( -1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0,  1.0);
    glEnd();
}
void RubicWidget::CubePiece3x3()//                                                |
{

  glColor3f(1.0, 0.5,0.0); //orange
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();


  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glColor3f(0.0, 0.0,1.0); //blue
    glBegin(GL_POLYGON);
      glVertex3f( 1.0, -1.0,  1.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glVertex3f( 1.0,  1.0, -1.0);
      glVertex3f( 1.0,  1.0,  1.0);
      glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,  -1.0,  1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glEnd();

  glColor3f(1.0, 1.0,0.0); //yellow
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,   1.0,  1.0);
    glVertex3f( -1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0,  1.0);
    glEnd();
}
void RubicWidget::CubePiece4x3()//                                                |
{

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();


  glColor3f(0.0,1.0,0.0); // green
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glColor3f(0.0,0.0,0.0); //black
    glBegin(GL_POLYGON);
      glVertex3f( 1.0, -1.0,  1.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glVertex3f( 1.0,  1.0, -1.0);
      glVertex3f( 1.0,  1.0,  1.0);
      glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,  -1.0,  1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glEnd();

  glColor3f(1.0, 1.0,0.0); //yellow
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,   1.0,  1.0);
    glVertex3f( -1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0,  1.0);
    glEnd();
}
void RubicWidget::CubePiece5x3()//                                                |
{

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();


  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glColor3f(0.0,0.0,0.0); //black
    glBegin(GL_POLYGON);
      glVertex3f( 1.0, -1.0,  1.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glVertex3f( 1.0,  1.0, -1.0);
      glVertex3f( 1.0,  1.0,  1.0);
      glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,  -1.0,  1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glEnd();

  glColor3f(1.0, 1.0,0.0); //yellow
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,   1.0,  1.0);
    glVertex3f( -1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0,  1.0);
    glEnd();
}
void RubicWidget::CubePiece6x3()//                                                |
{

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();


  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glColor3f(0.0, 0.0,1.0); //blue
    glBegin(GL_POLYGON);
      glVertex3f( 1.0, -1.0,  1.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glVertex3f( 1.0,  1.0, -1.0);
      glVertex3f( 1.0,  1.0,  1.0);
      glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,  -1.0,  1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glEnd();

  glColor3f(1.0, 1.0,0.0); //yellow
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,   1.0,  1.0);
    glVertex3f( -1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0,  1.0);
    glEnd();
}
void RubicWidget::CubePiece7x3()//                                                |
{

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.5,0.0,0.0); //red
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();


  glColor3f(0.0,1.0,0.0); // green
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glColor3f(0.0,0.0,0.0); //black
    glBegin(GL_POLYGON);
      glVertex3f( 1.0, -1.0,  1.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glVertex3f( 1.0,  1.0, -1.0);
      glVertex3f( 1.0,  1.0,  1.0);
      glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,  -1.0,  1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glEnd();

  glColor3f(1.0, 1.0,0.0); //yellow
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,   1.0,  1.0);
    glVertex3f( -1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0,  1.0);
    glEnd();
}
void RubicWidget::CubePiece8x3()//                                                |
{

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.5,0.0,0.0); //red
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();


  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glColor3f(0.0,0.0,0.0); //black
    glBegin(GL_POLYGON);
      glVertex3f( 1.0, -1.0,  1.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glVertex3f( 1.0,  1.0, -1.0);
      glVertex3f( 1.0,  1.0,  1.0);
      glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,  -1.0,  1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glEnd();

  glColor3f(1.0, 1.0,0.0); //yellow
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,   1.0,  1.0);
    glVertex3f( -1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0,  1.0);
    glEnd();
}
void RubicWidget::CubePiece9x3()//                                                |
{

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.5,0.0,0.0); //red
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();


  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glColor3f(0.0, 0.0,1.0); //blue
    glBegin(GL_POLYGON);
      glVertex3f( 1.0, -1.0,  1.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glVertex3f( 1.0,  1.0, -1.0);
      glVertex3f( 1.0,  1.0,  1.0);
      glEnd();

  glColor3f(0.0,0.0,0.0); //black
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,  -1.0,  1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glEnd();

  glColor3f(1.0, 1.0,0.0); //yellow
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,   1.0,  1.0);
    glVertex3f( -1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0, -1.0);
    glVertex3f(  1.0,   1.0,  1.0);
    glEnd();
}
//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄TESTING CUBE PIECE┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┤
void RubicWidget::TEST()//                                                        |
{

  glEnable(GL_TEXTURE_2D);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _image2.Width(), _image2.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, _image2.imageField());

  glColor3f(1.0, 0.5,0.0); //orange
  glBegin(GL_POLYGON);
    glTexCoord2f(0.0, 0.0);
    glVertex3f(-1.0, -1.0, 1.0);
    glTexCoord2f(1.1, 0.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glTexCoord2f(1.1, 1.1);
    glVertex3f( 1.0,  1.0, 1.0);
    glTexCoord2f(0.0, 1.1);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _image4.Width(), _image4.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, _image4.imageField());
  glColor3f(1.0, 1.0,0.0); //yellow
  glBegin(GL_POLYGON);
    glTexCoord2f(1.1, 0.0);
    glVertex3f(-1.0, -1.0, -1.0);
    glTexCoord2f(0.0, 0.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glTexCoord2f(0.0, 1.1);
    glVertex3f( 1.0,  1.0, -1.0);
    glTexCoord2f(1.1, 1.1);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _image3.Width(), _image3.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, _image3.imageField());
  glColor3f(0.0,1.0,0.0); // green
  glBegin(GL_POLYGON);
    glTexCoord2f(1.1, 0.0);
    glVertex3f( -1.0, -1.0,  1.0);
    glTexCoord2f(0.0, 0.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glTexCoord2f(0.0, 1.1);
    glVertex3f( -1.0,  1.0, -1.0);
    glTexCoord2f(1.1, 1.1);
    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _image5.Width(), _image5.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, _image5.imageField());
    glColor3f(0.0, 0.0,1.0); //blue
    glBegin(GL_POLYGON);
      glTexCoord2f(0.0, 0.0);
      glVertex3f( 1.0, -1.0,  1.0);
      glTexCoord2f(1.1, 0.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glTexCoord2f(1.1, 1.1);
      glVertex3f( 1.0,  1.0, -1.0);
      glTexCoord2f(0.0, 1.1);
      glVertex3f( 1.0,  1.0,  1.0);
      glEnd();

  glColor3f(1.0,1.0,1.0); //white
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,  -1.0,  1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glEnd();

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _image6.Width(), _image6.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, _image6.imageField());
  glColor3f(0.5,0.0,0.0); //red
  glBegin(GL_POLYGON);
      glTexCoord2f(0.0, 0.0);
    glVertex3f( -1.0,   1.0,  1.0);
          glTexCoord2f(1.1, 0.0);
    glVertex3f( -1.0,   1.0, -1.0);
              glTexCoord2f(1.1, 1.1);
    glVertex3f(  1.0,   1.0, -1.0);
              glTexCoord2f(0.0, 1.1);
    glVertex3f(  1.0,   1.0,  1.0);
    glEnd();
}
//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄RUBICES PIECES CREATED┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘




//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄X Y Z AXES┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┐
void RubicWidget::CylinderX(){
  glEnable(GL_LIGHTING);
  gluCylinder(pCylinderX,0.2,0.2,11.3,8,8);
  glDisable(GL_LIGHTING);
}
void RubicWidget::CylinderY(){
  glEnable(GL_LIGHTING);
  gluCylinder(pCylinderY,0.2,0.2,11.3,8,8);
  glDisable(GL_LIGHTING);
}
void RubicWidget::CylinderZ(){
  glEnable(GL_LIGHTING);
  gluCylinder(pCylinderZ,0.2,0.2,11.3,8,8);
  glDisable(GL_LIGHTING);
}
//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄X Y Z AXES┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘




//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄PAINT GL┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┐
void RubicWidget::paintGL()
  {
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the widget
      glShadeModel(GL_SMOOTH); //Smoothness
      glMatrixMode(GL_MODELVIEW);
	    glEnable(GL_DEPTH_TEST); //Enables Depth Testing
//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄PAINT GL┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘




// -------------------------------------------------------------_METHOD 1 -------------------------------------------------------------------------------------



//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄SLIDER ROTATION┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┐
    glPushMatrix();
        glRotatef(Yrotation*3.65, 0.0f, 1.0f, 0.0f);
        glTranslatef(0,0,0);
        glRotatef(Xrotation*3.65,1.0f,0.0f,0.0f);
        glTranslatef(0,0,0);


//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄SLIDER ROTATION┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘






//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄SIDE 1┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┐
//                                                                                |                                                                                |
    glPushMatrix();// THIRD ROTATION
          glTranslatef(0,0,0);
          glRotatef(Start*2,0,0,2*(Start*2));
          glTranslatef(0,0,0);

//                                                                                |
    glPushMatrix();// CUBE PIECE 1x1 (GREEN ORANGE WHITE)
        glTranslatef(-2.2,-2.2,-2.2);
        glRotatef(90,1,0,0);
        if (MixMoveA==1)
          {
            glTranslatef(2.2,2.2,0);
            glRotatef(A,0,0,A);
            glTranslatef(-2.2,2.2,0);
            glRotatef(90,0,0,-1);
          }
        if (MixMoveB==1)
          {
            glTranslatef(0,2.2,-2.2);
            glRotatef(B,B,0,0);
            glTranslatef(0,2.2,-2.2);
            glRotatef(180,1,0,0);
          }
        if (CrossMoveB == 1)
          {
            glTranslatef(2.2,0,-2.2);
            glRotatef(F*2,0,-F,0);
            glTranslatef(-2.2,0,2.2);
          }
        this->CubePiece1x1();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 2x1 (ORANGE WHITE)
        glTranslatef(0,-2.2,-2.2);
        glRotatef(90,1,0,0);
        if (MixMoveA==1)
          {
            glTranslatef(0,2.2,0);
            glRotatef(A,0,0,A);
            glTranslatef(-2.2,0,0);
            glRotatef(90,0,0,-1);
          }
        if (MixMoveC==1)
          {
            glTranslatef(0,2.2,0);
            glRotatef(C,0,0,-C);
            glTranslatef(-2.2,0,0);
            glRotatef(90,0,0,-1);
          }
        if (CrossMoveA == 1)
          {
            glTranslatef(0,2.2,0);
            glRotatef(E,0,0,-E);
            glTranslatef(2.2,0,0);
            glRotatef(90,0,0,1);
          }
        if (CrossMoveC == 1)
          {
            glTranslatef(0,2.2,-2.2);
            glRotatef(G,G,0,0);
            glTranslatef(0,-2.2,-2.2);
            glRotatef(90,1,0,0);
          }
        this->CubePiece2x1();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 3x1 (BLUE ORANGE WHITE)
        glTranslatef(2.2,-2.2,-2.2);
        glRotatef(90,1,0,0);
        if (MixMoveA==1)
          {
            glTranslatef(-2.2,2.2,0);
            glRotatef(A,0,0,A);
            glTranslatef(-2.2,-2.2,0);
            glRotatef(90,0,0,-1);
          }
        if (MixMoveB==1)
          {
            glTranslatef(0,2.2,-2.2);
            glRotatef(B,-B,0,0);
            glTranslatef(0,2.2,-2.2);
            glRotatef(180,1,0,0);
          }
        if (MixMoveC==1)
          {
            glTranslatef(-2.2,0,-2.2);
            glRotatef(C,0,C,0);
            glTranslatef(2.2,0,-2.2);
            glRotatef(90,0,1,0);
          }
        if (CrossMoveA == 1)
          {
            glTranslatef(-2.2,0,-2.2);
            glRotatef(E,0,E,0);
            glTranslatef(-2.2,0,2.2);
            glRotatef(90,0,-1,0);
          }
        if (CrossMoveD == 1)
          {
            glTranslatef(0,2.2,-2.2);
            glRotatef(H*2,-H,0,0);
            glTranslatef(0,-2.2,2.2);
          }
        this->CubePiece3x1();
    glPopMatrix();
//                                                                                |

    glPushMatrix();// CUBE PIECE 4x1 (GREEN WHITE)
        glTranslatef(-2.2,0,-2.2);
        glRotatef(90,1,0,0);
        if (MixMoveB==1)
          {
            glTranslatef(2.2,0,0);
            glRotatef(B,0,B,0);
            glTranslatef(2.2,0,0);
            glRotatef(180,0,1,0);
          }
        if (MixMoveC==1)
          {
            glTranslatef(0,2.2,0);
            glRotatef(C,C,0,0);
            glTranslatef(0,0,-2.2);
            glRotatef(90,1,0,0);
          }
        if (CrossMoveA == 1)
          {
            glTranslatef(0,2.2,0);
            glRotatef(E,E,0,0);
            glTranslatef(0,0,2.2);
            glRotatef(90,-1,0,0);
          }
        if (CrossMoveE == 1)
          {
            glTranslatef(2.2,2.2,0);
            glRotatef(I,0,0,I);
            glTranslatef(-2.2,2.2,0);
            glRotatef(90,0,0,-1);
          }
        this->CubePiece4x1();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 5x1 (WHITE)
        glTranslatef(0,0,-2.2);
        glRotatef(90,1,0,0);
        if (MixMoveB==1)
          {
            glTranslatef(0,0,0);
            glRotatef(B,0,B,0);
            glTranslatef(0,0,0);
            glRotatef(180,0,1,0);
          }
        if (MixMoveC==1)
          {
            glTranslatef(0,2.2,0);
            glRotatef(C*2,-C,0,0);
            glTranslatef(0,2.2,0);
            glRotatef(180,-1,0,0);
          }
        if (CrossMoveE == 1)
          {
            glTranslatef(0,2.2,0);
            glRotatef(I,0,0,I);
            glTranslatef(-2.2,0,0);
            glRotatef(90,0,0,-1);
          }
        this->CubePiece5x1();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 6x1 (BLUE WHITE)
        glTranslatef(2.2,0,-2.2);
        glRotatef(90,1,0,0);
        if (MixMoveB==1)
          {
            glTranslatef(-2.2,0,0);
            glRotatef(B,0,B,0);
            glTranslatef(-2.2,0,0);
            glRotatef(180,0,1,0);
          }
        if (CrossMoveB == 1)
          {
            glTranslatef(0,2.2,0);
            glRotatef(F*2,F,0,0);
            glTranslatef(0,-2.2,0);
          }
        if (CrossMoveE == 1)
          {
            glTranslatef(-2.2,2.2,0);
            glRotatef(I,0,0,I);
            glTranslatef(-2.2,-2.2,0);
            glRotatef(90,0,0,-1);
          }
        this->CubePiece6x1();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 7x1 (GREEN RED WHITE)
        glTranslatef(-2.2,2.2,-2.2);
        glRotatef(90,1,0,0);
        if (MixMoveA==1)
          {
            glTranslatef(2.2,2.2,0);
            glRotatef(A,0,0,A);
            glTranslatef(-2.2,2.2,0);
            glRotatef(90,0,0,-1);
          }
        if (MixMoveB==1)
          {
            glTranslatef(0,2.2,2.2);
            glRotatef(B,B,0,0);
            glTranslatef(0,2.2,2.2);
            glRotatef(180,1,0,0);
          }
        if (CrossMoveB == 1)
          {
            glTranslatef(2.2,0,2.2);
            glRotatef(F*2,0,-F,0);
            glTranslatef(-2.2,0,-2.2);
          }
        this->CubePiece7x1();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 8x1 (RED WHITE)
        glTranslatef(0,2.2,-2.2);
        glRotatef(90,1,0,0);
        if (MixMoveA==1)
          {
            glTranslatef(0,2.2,0);
            glRotatef(A,0,0,A);
            glTranslatef(-2.2,0,0);
            glRotatef(90,0,0,-1);
          }
        if (CrossMoveB == 1)
          {
            glTranslatef(0,2.2,0);
            glRotatef(F*2,0,0,-F);
            glTranslatef(0,-2.2,0);
          }
        if (CrossMoveC == 1)
          {
            glTranslatef(0,2.2,2.2);
            glRotatef(G,G,0,0);
            glTranslatef(0,2.2,-2.2);
            glRotatef(90,1,0,0);
          }
        this->CubePiece8x1();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 9x1 (RED BLUE WHITE)
        glTranslatef(2.2,2.2,-2.2);
        glRotatef(90,1,0,0);
        if (MixMoveA==1)
          {
            glTranslatef(-2.2,2.2,0);
            glRotatef(A,0,0,A);
            glTranslatef(-2.2,-2.2,0);
            glRotatef(90,0,0,-1);
          }
        if (MixMoveB==1)
          {
            glTranslatef(0,2.2,2.2);
            glRotatef(B,-B,0,0);
            glTranslatef(0,2.2,2.2);
            glRotatef(180,1,0,0);
          }
        if (MixMoveC==1)
          {
            glTranslatef(-2.2,0,2.2);
            glRotatef(C,0,C,0);
            glTranslatef(-2.2,0,-2.2);
            glRotatef(90,0,1,0);
          }
        if (CrossMoveA == 1)
          {
            glTranslatef(-2.2,0,2.2);
            glRotatef(E,0,E,0);
            glTranslatef(2.2,0,2.2);
            glRotatef(90,0,-1,0);
          }
        if (CrossMoveD == 1)
          {
            glTranslatef(0,2.2,2.2);
            glRotatef(H*2,-H,0,0);
            glTranslatef(0,-2.2,-2.2);
          }
        this->CubePiece9x1();
    glPopMatrix();
//                                                                                |
    glPopMatrix();// THIRD ROTATION
//                                                                                |
//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄SIDE 1┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘






//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄SIDE 2┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┐
//                                                                                |
    glPushMatrix();// SECOND ROTATION
          glTranslatef(0,0,0);
          glRotatef(Start,0,0,-(Start*2));
          glTranslatef(0,0,0);
        if (CrossMoveF==1)
        {
          glTranslatef(0,0,0);
          glRotatef(180,1,0,0);
          glRotatef(J,J,0,0);
          glTranslatef(0,0,0);
          SPIN2=1;
        }

//                                                                                |
    glPushMatrix();// CUBE PIECE 1x2 (GREEN ORANGE)
        glTranslatef(-2.2,-2.2,0);
        glRotatef(90,1,0,0);
        if (MixMoveA==1)
          {
            glTranslatef(0,0,-2.2);
            glRotatef(A,-A*2,0,0);
            glTranslatef(0,-2.2,0);
            glRotatef(90,1,0,0);
          }
        if (MixMoveB==1)
          {
            glTranslatef(2.2,0,-2.2);
            glRotatef(B,0,0,-B);
            glTranslatef(2.2,0,2.2);
            glRotatef(180,0,0,1);
          }
        if (MixMoveC==1)
          {
            glTranslatef(2.2,0,-2.2);
            glRotatef(C*2,0,-C,0);
            glTranslatef(2.2,0,-2.2);
            glRotatef(180,0,-1,0);
          }
        if (CrossMoveD == 1)
          {
            glTranslatef(2.2,0,0);
            glRotatef(H*2,0,0,-H);
            glTranslatef(-2.2,0,0);
          }
        this->CubePiece1x2();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 2x2 (ORANGE)
        glTranslatef(0,-2.2,0);
        glRotatef(90,1,0,0);
        if (MixMoveC==1)
          {
            glTranslatef(0,0,-2.2);
            glRotatef(C*2,-C,0,0);
            glTranslatef(0,0,-2.2);
            glRotatef(180,-1,0,0);
          }
        if (CrossMoveC == 1)
          {
            glTranslatef(0,0,-2.2);
            glRotatef(G,0,G,0);
            glTranslatef(2.2,0,0);
            glRotatef(90,0,1,0);
          }
        if (CrossMoveE == 1)
          {
            glTranslatef(0,0,-2.2);
            glRotatef(I,-I,0,0);
            glTranslatef(0,-2.2,0);
            glRotatef(90,1,0,0);
          }
      this->CubePiece2x2();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 3x2 (BLUE ORANGE)
        glTranslatef(2.2,-2.2,0);
        glRotatef(90,1,0,0);
        if (MixMoveA==1)
          {
            glTranslatef(0,0,-2.2);
            glRotatef(A,-A,0,0);
            glTranslatef(0,-2.2,0);
            glRotatef(90,1,0,0);
          }
        if (MixMoveB==1)
          {
            glTranslatef(-2.2,0,0);
            glRotatef(B,0,0,-B);
            glTranslatef(-2.2,0,0);
            glRotatef(180,0,0,1);
          }
        if (MixMoveC==1)
          {
            glTranslatef(-2.2,0,-2.2);
            glRotatef(C*2,0,-C,0);
            glTranslatef(-2.2,0,-2.2);
            glRotatef(180,0,-1,0);
          }
        if (CrossMoveD == 1)
          {
            glTranslatef(-2.2,0,0);
            glRotatef(H*2,0,0,-H);
            glTranslatef(2.2,0,0);
          }
        this->CubePiece3x2();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 4x2 (GREEN)
        glTranslatef(-2.2,0,0);
        glRotatef(90,1,0,0);
        if (MixMoveA==1)
          {
            glTranslatef(0,0,0);
            glRotatef(A,-A,0,0);
            glTranslatef(0,0,0);
            glRotatef(90,1,0,0);
          }
        if (MixMoveC==1)
          {
            glTranslatef(0,0,0);
            glRotatef(C,C,0,0);
            glTranslatef(0,0,0);
          }
        if (CrossMoveA == 1)
          {
            glTranslatef(0,0,0);
            glRotatef(E,E,0,0);
            glTranslatef(0,0,0);
            glRotatef(90,1,0,0);
          }
        if (CrossMoveC == 1)
          {
            glTranslatef(2.2,0,0);
            glRotatef(G,0,G,0);
            glTranslatef(0,0,2.2);
            glRotatef(90,0,1,0);
          }
        this->CubePiece4x2();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 5x2 (-BLACK-)
        glTranslatef(0,0,0);
        glRotatef(90,1,0,0);
        this->CubePiece5x2();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 6x2 (BLUE)
        glTranslatef(2.2,0,0);
        glRotatef(90,1,0,0);
        if (MixMoveA==1)
          {
            glTranslatef(0,0,0);
            glRotatef(A,-A,0,0);
            glTranslatef(0,0,0);
            glRotatef(90,1,0,0);
          }
        if (CrossMoveB == 1)
          {
            glTranslatef(0,0,0);
            glRotatef(F*2,F,0,0);
            glTranslatef(0,0,0);
          }
        if (CrossMoveC == 1)
          {
            glTranslatef(-2.2,0,0);
            glRotatef(G,0,0,G);
            glTranslatef(0,2.2,0);
            glRotatef(90,0,0,1);
          }
        this->CubePiece6x2();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 7x2 (GREEN RED)
        glTranslatef(-2.2,2.2,0);
        glRotatef(90,1,0,0);
        if (MixMoveA==1)
          {
            glTranslatef(0,0,2.2);
            glRotatef(A,-A,0,0);
            glTranslatef(0,2.2,0);
            glRotatef(90,1,0,0);
          }
        if (MixMoveB==1)
          {
            glTranslatef(2.2,0,0);
            glRotatef(B,0,0,B);
            glTranslatef(2.2,0,0);
            glRotatef(180,0,0,1);
          }
        if (MixMoveC==1)
          {
            glTranslatef(2.2,0,2.2);
            glRotatef(C*2,0,C,0);
            glTranslatef(2.2,0,2.2);
            glRotatef(180,0,1,0);
          }
        this->CubePiece7x2();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 8x2 (RED)
        glTranslatef(0,2.2,0);
        glRotatef(90,1,0,0);
        if (MixMoveC==1)
          {
            glTranslatef(0,0,2.2);
            glRotatef(C*2,-C,0,0);
            glTranslatef(0,0,2.2);
            glRotatef(180,0,-1,0);
          }
        if (CrossMoveC == 1)
          {
            glTranslatef(0,0,2.2);
            glRotatef(G,0,-G,0);
            glTranslatef(2.2,0,0);
            glRotatef(90,0,-1,0);
          }
        if (CrossMoveE == 1)
          {
            glTranslatef(0,0,2.2);
            glRotatef(I,I,0,0);
            glTranslatef(0,-2.2,0);
            glRotatef(90,-1,0,0);
          }
        this->CubePiece8x2();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 9x2 (RED BLUE)
        glTranslatef(2.2,2.2,0);
        glRotatef(90,1,0,0);
        if (MixMoveA==1)
          {
            glTranslatef(0,0,2.2);
            glRotatef(A,-A,0,0);
            glTranslatef(0,2.2,0);
            glRotatef(90,1,0,0);
          }
        if (MixMoveB==1)
          {
            glTranslatef(-2.2,0,0);
            glRotatef(B,0,0,B);
            glTranslatef(-2.2,0,0);
            glRotatef(180,0,0,1);
          }
        if (MixMoveC==1)
          {
            glTranslatef(-2.2,0,2.2);
            glRotatef(C*2,0,C,0);
            glTranslatef(-2.2,0,2.2);
            glRotatef(180,0,-1,0);
          }
        this->CubePiece9x2();
    glPopMatrix();
//                                                                                |
    glPopMatrix();// SECOND ROTATION
//                                                                                |
//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄SIDE 2┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘






//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄SIDE 3┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┐
//                                                                                |
    glPushMatrix();// FIRST ROTATION
            glTranslatef(0,0,0);
            glRotatef(Start,0,0,Start*2);
            glTranslatef(0,0,0);
        if (CrossMoveG == 1)
          {
            glTranslatef(0,0,0);
            glRotatef(K,K,0,0);
            glRotatef(90,1,0,0);
            glTranslatef(0,0,0);
            SPIN3 =1;
          }
//                                                                                |
    glPushMatrix();// CUBE PIECE 1x3 (GREEN ORANGE YELLOW)
        glTranslatef(-2.2,-2.2,2.2);
        glRotatef(90,1,0,0);
        if (MixMoveA ==1)
          {
            glTranslatef(0,-2.2,-2.2);
            glRotatef(A,A,0,0);
            glTranslatef(0,2.2,-2.2);
            glRotatef(90,-1,0,0);
          }
        if (MixMoveB==1)
          {
            glTranslatef(2.2,-2.2,0);
            glRotatef(B,0,0,-B);
            glTranslatef(2.2,-2.2,0);
            glRotatef(180,0,0,1);
          }
        if (MixMoveC==1)
          {
            glTranslatef(2.2,0,-2.2);
            glRotatef(C,0,-C,0);
            glTranslatef(-2.2,0,-2.2);
            glRotatef(90,0,-1,0);
          }
        if (CrossMoveA == 1)
          {
            glTranslatef(2.2,0,-2.2);
            glRotatef(E,0,-E,0);
            glTranslatef(2.2,0,2.2);
            glRotatef(90,0,1,0);
          }
        this->CubePiece1x3();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 2x3 (ORANGE YELLOW)
        glTranslatef(0,-2.2,2.2);
        glRotatef(90,1,0,0);
        if (MixMoveB==1)
          {
            glTranslatef(0,0,-2.2);
            glRotatef(B,0,-B,0);
            glTranslatef(0,0,-2.2);
            glRotatef(180,0,1,0);
          }
        if (CrossMoveB == 1)
          {
            glTranslatef(0,-2.2,0);
            glRotatef(F*2,0,0,F);
            glTranslatef(0,2.2,0);
          }
        if (CrossMoveD == 1)
          {
            glTranslatef(0,0,-2.2);
            glRotatef(H*2,0,-H,0);
            glTranslatef(0,0,2.2);
          }
        if (CrossMoveE == 1)
          {
            glTranslatef(0,-2.2,-2.2);
            glRotatef(I,I,0,0);
            glTranslatef(0,2.2,-2.2);
            glRotatef(90,-1,0,0);
          }
        this->CubePiece2x3();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 3x3 (BLUE ORANGE YELLOW)
        glTranslatef(2.2,-2.2,2.2);
        glRotatef(90,1,0,0);
        if (MixMoveA==1)
          {
            glTranslatef(0,-2.2,-2.2);
            glRotatef(A,A,0,0);
            glTranslatef(0,2.2,-2.2);
            glRotatef(90,-1,0,0);
          }
        if (MixMoveB==1)
          {
            glTranslatef(-2.2,-2.2,0);
            glRotatef(B,0,0,-B);
            glTranslatef(-2.2,-2.2,0);
            glRotatef(180,0,0,1);
          }
        if (MixMoveC==1)
          {
            glTranslatef(-2.2,0,-2.2);
            glRotatef(C,0,-C,0);
            glTranslatef(-2.2,0,2.2);
            glRotatef(90,0,-1,0);
          }
        if (CrossMoveA == 1)
          {
            glTranslatef(-2.2,0,-2.2);
            glRotatef(E,0,-E,0);
            glTranslatef(2.2,0,-2.2);
            glRotatef(90,0,1,0);
          }
        this->CubePiece3x3();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 4x3 (GREEN YELLOW)
        glTranslatef(-2.2,0,2.2);
        glRotatef(90,1,0,0);
        if (MixMoveA==1)
          {
            glTranslatef(0,-2.2,0);
            glRotatef(A,A,0,0);
            glTranslatef(0,0,-2.2);
            glRotatef(90,-1,0,0);
          }
        if (CrossMoveB == 1)
          {
            glTranslatef(0,-2.2,0);
            glRotatef(F*2,-F,0,0);
            glTranslatef(0,2.2,0);
          }
        if (CrossMoveC == 1)
          {
            glTranslatef(2.2,-2.2,0);
            glRotatef(G,0,0,-G);
            glTranslatef(2.2,2.2,0);
            glRotatef(90,0,0,-1);
          }
        this->CubePiece4x3();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 5x3 (YELLOW)
        glTranslatef(0,0,2.2);
        glRotatef(90,1,0,0);
        if (MixMoveB==1)
          {
            glTranslatef(0,0,0);
            glRotatef(B,0,-B,0);
            glTranslatef(0,0,0);
            //glRotatef(90,1,0,0);
          }
        if (MixMoveC==1)
          {
            glTranslatef(0,-2.2,0);
            glRotatef(C*2,0,0,C);
            glTranslatef(0,-2.2,0);
            glRotatef(180,-1,0,0);
          }
        if (CrossMoveD == 1)
          {
            glTranslatef(0,0,0);
            glRotatef(H*2,0,-H,0);
            glTranslatef(0,0,0);
          }
        if (CrossMoveE == 1)
          {
            glTranslatef(0,-2.2,0);
            glRotatef(I,I,0,0);
            glTranslatef(0,0,-2.2);
            glRotatef(90,-1,0,0);
          }
        this->CubePiece5x3();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 6x3 (BLUE YELLOW)
        glTranslatef(2.2,0,2.2);
        glRotatef(90,1,0,0);
        if (MixMoveA==1)
          {
            glTranslatef(0,-2.2,0);
            glRotatef(A,A,0,0);
            glTranslatef(0,0,-2.2);
            glRotatef(90,-1,0,0);
          }
        if (MixMoveC==1)
          {
            glTranslatef(0,-2.2,0);
            glRotatef(C,-C,0,0);
            glTranslatef(0,0,-2.2);
            glRotatef(90,-1,0,0);
          }
        if (CrossMoveA == 1)
          {
            glTranslatef(0,-2.2,0);
            glRotatef(E,-E,0,0);
            glTranslatef(0,0,2.2);
            glRotatef(90,1,0,0);
          }
        if (CrossMoveC == 1)
          {
            glTranslatef(-2.2,-2.2,0);
            glRotatef(G,0,0,-G);
            glTranslatef(2.2,-2.2,0);
            glRotatef(90,0,0,-1);
          }
        this->CubePiece6x3();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 7x3 (GREEN RED YELLOW)
        glTranslatef(-2.2,2.2,2.2);
        glRotatef(90,1,0,0);
        if (MixMoveA==1)
          {
            glTranslatef(0,-2.2,2.2);
            glRotatef(A,A,0,0);
            glTranslatef(0,-2.2,-2.2);
            glRotatef(90,-1,0,0);
          }
        if (MixMoveB==1)
          {
            glTranslatef(2.2,-2.2,0);
            glRotatef(B,0,0,B);
            glTranslatef(2.2,-2.2,0);
            glRotatef(180,0,0,1);
          }
        if (CrossMoveB == 1)
          {
            glTranslatef(2.2,0,2.2);
            glRotatef(F*2,0,F,0);
            glTranslatef(-2.2,0,-2.2);
          }
        if (CrossMoveD == 1)
          {
            glTranslatef(2.2,-2.2,0);
            glRotatef(H*2,0,0,H);
            glTranslatef(-2.2,2.2,0);
          }
        this->CubePiece7x3();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 8x3 (RED YELLOW)
        glTranslatef(0,2.2,2.2);
        glRotatef(90,1,0,0);
        if (MixMoveB==1)
          {
            glTranslatef(0,0,2.2);
            glRotatef(B,0,-B,0);
            glTranslatef(0,0,2.2);
            glRotatef(180,0,1,0);
          }
        if (MixMoveC==1)
          {
            glTranslatef(0,-2.2,0);
            glRotatef(C,0,0,C);
            glTranslatef(-2.2,0,0);
            glRotatef(90,0,0,1);
          }
        if (CrossMoveA == 1)
          {
            glTranslatef(0,-2.2,0);
            glRotatef(E,0,0,E);
            glTranslatef(2.2,0,0);
            glRotatef(90,0,0,-1);
          }
        if (CrossMoveD == 1)
          {
            glTranslatef(0,0,2.2);
            glRotatef(H*2,0,-H,0);
            glTranslatef(0,0,-2.2);
          }
        if (CrossMoveE == 1)
          {
            glTranslatef(0,-2.2,2.2);
            glRotatef(I,I,0,0);
            glTranslatef(0,-2.2,-2.2);
            glRotatef(90,-1,0,0);
          }
        this->CubePiece8x3();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 9x3 (RED BLUE YELLOW)
        glTranslatef(2.2,2.2,2.2);
        glRotatef(90,1,0,0);
        if (MixMoveA==1)
          {
            glTranslatef(0,-2.2,2.2);
            glRotatef(A,A,0,0);
            glTranslatef(0,-2.2,-2.2);
            glRotatef(90,-1,0,0);
          }
        if (MixMoveB==1)
          {
            glTranslatef(-2.2,-2.2,0);
            glRotatef(B,0,0,B);
            glTranslatef(-2.2,-2.2,0);
            glRotatef(180,0,0,1);
          }
        if (CrossMoveB == 1)
          {
            glTranslatef(-2.2,0,2.2);
            glRotatef(F*2,0,F,0);
            glTranslatef(2.2,0,-2.2);
          }
        if (CrossMoveD == 1)
          {
            glTranslatef(-2.2,-2.2,0);
            glRotatef(H*2,0,0,H);
            glTranslatef(2.2,2.2,0);
          }
        this->CubePiece9x3();
    glPopMatrix();
//                                                                                |
    glPopMatrix();// FIRST ROTATION
//                                                                                |
//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄SIDE 3┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘



//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄SLIDER ROTATION┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┐
    glPopMatrix();                     //                                         |
//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄SLIDER ROTATION┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘




//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄TESTING PIECE┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┐
    glPushMatrix();
    glScalef(4,4,4);
      glTranslatef(-3,3,0);
      glRotatef(7.1,0,1,0);
      glRotatef(7.1,-1,0,0);
      if(MixMoveD ==1)
      {
        glRotatef(90,0,1,0);
      }
        if(SPIN3 ==1)
        {
          glRotatef(90,0,1,0);
        }

      this->TEST();
    glPopMatrix();
//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄TESTING PIECE┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘

//*/----------------------------------------------------------------------METHOD 1---------------------------------------------------------------




/*//----------------------------------------------------------------------METHOD 2-----------------------------------------------------------------

//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄SLIDER ROTATION┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┐
    glPushMatrix();
        glRotatef(Yrotation*3.65, 0.0f, 1.0f, 0.0f);
        glTranslatef(0,0,0);
        glRotatef(Xrotation*3.65,1.0f,0.0f,0.0f);
        glTranslatef(0,0,0);


//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄SLIDER ROTATION┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘





//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄SIDE 1┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┐
//                                                                                |                                                                                |
    glPushMatrix();// THIRD ROTATION


//                                                                                |
    glPushMatrix();// CUBE PIECE 1x1 (GREEN ORANGE WHITE)
        glTranslatef(-2.2,-2.2,-2.2);
        glRotatef(90,1,0,0);
        if (MixMoves == 1)
        {
        glTranslatef(POSX1x1,POSY1x1,POSZ1x1);
        glRotatef(Angle1x1,RX1x1,RY1x1,RZ1x1);
        glTranslatef(POS2X1x1,POS2Y1x1,POS2Z1x1);
        glRotatef(90,-RX1x1,-RY1x1,-RZ1x1);
        }
        this->CubePiece1x1();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 2x1 (ORANGE WHITE)
        glTranslatef(0,-2.2,-2.2);
        glRotatef(90,1,0,0);
        if (MixMoves == 1)
        {
        glTranslatef(POSX2x1,POSY2x1,POSZ2x1);
        glRotatef(Angle2x1,RX2x1,RY2x1,RZ2x1);
        glTranslatef(POS2X2x1,POS2Y2x1,POS2Z2x1);
        glRotatef(90,-RX2x1,-RY2x1,-RZ2x1);
        }
        this->CubePiece2x1();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 3x1 (BLUE ORANGE WHITE)
        glTranslatef(2.2,-2.2,-2.2);
        glRotatef(90,1,0,0);
        if (MixMoves == 1)
        {
        glTranslatef(POSX3x1,POSY3x1,POSZ3x1);
        glRotatef(Angle3x1,RX3x1,RY3x1,RZ3x1);
        glTranslatef(POS2X3x1,POS2Y3x1,POS2Z3x1);
        glRotatef(90,-RX3x1,-RY3x1,-RZ3x1);
        }
        this->CubePiece3x1();
    glPopMatrix();
//                                                                                |

    glPushMatrix();// CUBE PIECE 4x1 (GREEN WHITE)
        glTranslatef(-2.2,0,-2.2);
        glRotatef(90,1,0,0);
        if (MixMoves == 1)
        {
        glTranslatef(POSX4x1,POSY4x1,POSZ4x1);
        glRotatef(Angle4x1,RX4x1,RY4x1,RZ4x1);
        glTranslatef(POS2X4x1,POS2Y4x1,POS2Z4x1);
        glRotatef(90,-RX4x1,-RY4x1,-RZ4x1);
        }
        this->CubePiece4x1();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 5x1 (WHITE)
        glTranslatef(0,0,-2.2);
        glRotatef(90,1,0,0);
        if (MixMoves == 1)
        {
        glTranslatef(POSX5x1,POSY5x1,POSZ5x1);
        glRotatef(Angle5x1,RX5x1,RY5x1,RZ5x1);
        glTranslatef(POS2X5x1,POS2Y5x1,POS2Z5x1);
        glRotatef(90,-RX5x1,-RY5x1,-RZ5x1);
        }
        this->CubePiece5x1();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 6x1 (BLUE WHITE)
        glTranslatef(2.2,0,-2.2);
        glRotatef(90,1,0,0);
        if (MixMoves == 1)
        {
        glTranslatef(POSX6x1,POSY6x1,POSZ6x1);
        glRotatef(Angle6x1,RX6x1,RY6x1,RZ6x1);
        glTranslatef(POS2X6x1,POS2Y6x1,POS2Z6x1);
        glRotatef(90,-RX6x1,-RY6x1,-RZ6x1);
        }
        this->CubePiece6x1();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 7x1 (GREEN RED WHITE)
        glTranslatef(-2.2,2.2,-2.2);
        glRotatef(90,1,0,0);
        if (MixMoves == 1)
        {
        glTranslatef(POSX7x1,POSY7x1,POSZ7x1);
        glRotatef(Angle7x1,RX7x1,RY7x1,RZ7x1);
        glTranslatef(POS2X7x1,POS2Y7x1,POS2Z7x1);
        glRotatef(90,-RX7x1,-RY7x1,-RZ7x1);
        }
        this->CubePiece7x1();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 8x1 (RED WHITE)
        glTranslatef(0,2.2,-2.2);
        glRotatef(90,1,0,0);
        if (MixMoves == 1)
        {
        glTranslatef(POSX8x1,POSY8x1,POSZ8x1);
        glRotatef(Angle8x1,RX8x1,RY8x1,RZ8x1);
        glTranslatef(POS2X8x1,POS2Y8x1,POS2Z8x1);
        glRotatef(90,-RX8x1,-RY8x1,-RZ8x1);
        }
        this->CubePiece8x1();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 9x1 (RED BLUE WHITE)
        glTranslatef(2.2,2.2,-2.2);
        glRotatef(90,1,0,0);
        if (MixMoves == 1)
        {
        glTranslatef(POSX9x1,POSY9x1,POSZ9x1);
        glRotatef(Angle9x1,RX9x1,RY9x1,RZ9x1);
        glTranslatef(POS2X9x1,POS2Y9x1,POS2Z9x1);
        glRotatef(90,-RX9x1,-RY9x1,-RZ9x1);
        }
        this->CubePiece9x1();
    glPopMatrix();
//                                                                                |

    glPopMatrix();// THIRD ROTATION
//                                                                                |
//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄SIDE 1┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘






//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄SIDE 2┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┐
//                                                                                |
    glPushMatrix();// SECOND ROTATION
          glTranslatef(0,0,0);
          glRotatef(Start,0,0,-(Start*2));
          glTranslatef(0,0,0);


//                                                                                |
    glPushMatrix();// CUBE PIECE 1x2 (GREEN ORANGE)
        glTranslatef(-2.2,-2.2,0);
        glRotatef(90,1,0,0);
        if (MixMoves == 1)
        {
        glTranslatef(POSX1x2,POSY1x2,POSZ1x2);
        glRotatef(Angle1x2,RX1x2,RY1x2,RZ1x2);
        glTranslatef(POS2X1x2,POS2Y1x2,POS2Z1x2);
        glRotatef(90,-RX1x2,-RY1x2,-RZ1x2);
        }
        this->CubePiece1x2();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 2x2 (ORANGE)
        glTranslatef(0,-2.2,0);
        glRotatef(90,1,0,0);
        if (MixMoves == 1)
        {
        glTranslatef(POSX2x2,POSY2x2,POSZ2x2);
        glRotatef(Angle2x2,RX2x2,RY2x2,RZ2x2);
        glTranslatef(POS2X2x2,POS2Y2x2,POS2Z2x2);
        glRotatef(90,-RX2x2,-RY2x2,-RZ2x2);
        }
      this->CubePiece2x2();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 3x2 (BLUE ORANGE)
        glTranslatef(2.2,-2.2,0);
        glRotatef(90,1,0,0);
        if (MixMoves == 1)
        {
        glTranslatef(POSX3x2,POSY3x2,POSZ3x2);
        glRotatef(Angle3x2,RX3x2,RY3x2,RZ3x2);
        glTranslatef(POS2X3x2,POS2Y3x2,POS2Z3x2);
        glRotatef(90,-RX3x2,-RY3x2,-RZ3x2);
        }
        this->CubePiece3x2();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 4x2 (GREEN)
        glTranslatef(-2.2,0,0);
        glRotatef(90,1,0,0);
        if (MixMoves == 1)
        {
        glTranslatef(POSX4x2,POSY4x2,POSZ4x2);
        glRotatef(Angle4x2,RX4x2,RY4x2,RZ4x2);
        glTranslatef(POS2X4x2,POS2Y4x2,POS2Z4x2);
        glRotatef(90,-RX4x2,-RY4x2,-RZ4x2);
        }
        this->CubePiece4x2();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 5x2 (-BLACK-)
        glTranslatef(0,0,0);
        glRotatef(90,1,0,0);
        if (MixMoves == 1)
        {
        glTranslatef(POSX5x2,POSY5x2,POSZ5x2);
        glRotatef(Angle5x2,RX5x2,RY5x2,RZ5x2);
        glTranslatef(POS2X5x2,POS2Y5x2,POS2Z5x2);
        glRotatef(90,-RX5x2,-RY5x2,-RZ5x2);
        }
        this->CubePiece5x2();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 6x2 (BLUE)
        glTranslatef(2.2,0,0);
        glRotatef(90,1,0,0);
        if (MixMoves == 1)
        {
        glTranslatef(POSX6x2,POSY6x2,POSZ6x2);
        glRotatef(Angle6x2,RX6x2,RY6x2,RZ6x2);
        glTranslatef(POS2X6x2,POS2Y6x2,POS2Z6x2);
        glRotatef(90,-RX6x2,-RY6x2,-RZ6x2);
        }
        this->CubePiece6x2();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 7x2 (GREEN RED)
        glTranslatef(-2.2,2.2,0);
        glRotatef(90,1,0,0);
        if (MixMoves == 1)
        {
        glTranslatef(POSX7x2,POSY7x2,POSZ7x2);
        glRotatef(Angle7x2,RX7x2,RY7x2,RZ7x2);
        glTranslatef(POS2X7x2,POS2Y7x2,POS2Z7x2);
        glRotatef(90,-RX7x2,-RY7x2,-RZ7x2);
        }
        this->CubePiece7x2();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 8x2 (RED)
        glTranslatef(0,2.2,0);
        glRotatef(90,1,0,0);
        if (MixMoves == 1)
        {
        glTranslatef(POSX8x2,POSY8x2,POSZ8x2);
        glRotatef(Angle8x2,RX8x2,RY8x2,RZ8x2);
        glTranslatef(POS2X8x2,POS2Y8x2,POS2Z8x2);
        glRotatef(90,-RX8x2,-RY8x2,-RZ8x2);
        }
        this->CubePiece8x2();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 9x2 (RED BLUE)
        glTranslatef(2.2,2.2,0);
        glRotatef(90,1,0,0);
        if (MixMoves == 1)
        {
        glTranslatef(POSX9x2,POSY9x2,POSZ9x2);
        glRotatef(Angle9x2,RX9x2,RY9x2,RZ9x2);
        glTranslatef(POS2X9x2,POS2Y9x2,POS2Z9x2);
        glRotatef(90,-RX9x2,-RY9x2,-RZ9x2);
        }
        this->CubePiece9x2();
    glPopMatrix();
//                                                                                |
    glPopMatrix();// SECOND ROTATION
//                                                                                |
//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄SIDE 2┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘






//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄SIDE 3┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┐
//                                                                                |
    glPushMatrix();// FIRST ROTATION
            glTranslatef(0,0,0);
            glRotatef(Start,0,0,Start*2);
            glTranslatef(0,0,0);

//                                                                                |
    glPushMatrix();// CUBE PIECE 1x3 (GREEN ORANGE YELLOW)
        glTranslatef(-2.2,-2.2,2.2);
        glRotatef(90,1,0,0);
        if (MixMoves == 1)
        {
        glTranslatef(POSX1x3,POSY1x3,POSZ1x3);
        glRotatef(Angle1x3,RX1x3,RY1x3,RZ1x3);
        glTranslatef(POS2X1x3,POS2Y1x3,POS2Z1x3);
        glRotatef(90,-RX1x3,-RY1x3,-RZ1x3);
        }
        this->CubePiece1x3();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 2x3 (ORANGE YELLOW)
        glTranslatef(0,-2.2,2.2);
        glRotatef(90,1,0,0);
        if (MixMoves == 1)
        {
        glTranslatef(POSX2x3,POSY2x3,POSZ2x3);
        glRotatef(Angle2x3,RX2x3,RY2x3,RZ2x3);
        glTranslatef(POS2X2x3,POS2Y2x3,POS2Z2x3);
        glRotatef(90,-RX2x3,-RY2x3,-RZ2x3);
        }
        this->CubePiece2x3();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 3x3 (BLUE ORANGE YELLOW)
        glTranslatef(2.2,-2.2,2.2);
        glRotatef(90,1,0,0);
        if (MixMoves == 1)
        {
        glTranslatef(POSX3x3,POSY3x3,POSZ3x3);
        glRotatef(Angle3x3,RX3x3,RY3x3,RZ3x3);
        glTranslatef(POS2X3x3,POS2Y3x3,POS2Z3x3);
        glRotatef(90,-RX3x3,-RY3x3,-RZ3x3);
        }
        this->CubePiece3x3();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 4x3 (GREEN YELLOW)
        glTranslatef(-2.2,0,2.2);
        glRotatef(90,1,0,0);
        if (MixMoves == 1)
        {
        glTranslatef(POSX4x3,POSY4x3,POSZ4x3);
        glRotatef(Angle4x3,RX4x3,RY4x3,RZ4x3);
        glTranslatef(POS2X4x3,POS2Y4x3,POS2Z4x3);
        glRotatef(90,-RX4x3,-RY4x3,-RZ4x3);
        }
        this->CubePiece4x3();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 5x3 (YELLOW)
        glTranslatef(0,0,2.2);
        glRotatef(90,1,0,0);
        if (MixMoves == 1)
        {
        glTranslatef(POSX5x3,POSY5x3,POSZ5x3);
        glRotatef(Angle5x3,RX5x3,RY5x3,RZ5x3);
        glTranslatef(POS2X5x3,POS2Y5x3,POS2Z5x3);
        glRotatef(90,-RX5x3,-RY5x3,-RZ5x3);
        }
        this->CubePiece5x3();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 6x3 (BLUE YELLOW)
        glTranslatef(2.2,0,2.2);
        glRotatef(90,1,0,0);
        if (MixMoves == 1)
        {
        glTranslatef(POSX6x3,POSY6x3,POSZ6x3);
        glRotatef(Angle6x3,RX6x3,RY6x3,RZ6x3);
        glTranslatef(POS2X6x3,POS2Y6x3,POS2Z6x3);
        glRotatef(90,-RX6x3,-RY6x3,-RZ6x3);
        }
        this->CubePiece6x3();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 7x3 (GREEN RED YELLOW)
        glTranslatef(-2.2,2.2,2.2);
        glRotatef(90,1,0,0);
        if (MixMoves == 1)
        {
        glTranslatef(POSX7x3,POSY7x3,POSZ7x3);
        glRotatef(Angle7x3,RX7x3,RY7x3,RZ7x3);
        glTranslatef(POS2X7x3,POS2Y7x3,POS2Z7x3);
        glRotatef(90,-RX7x3,-RY7x3,-RZ7x3);
        }
        this->CubePiece7x3();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 8x3 (RED YELLOW)
        glTranslatef(0,2.2,2.2);
        glRotatef(90,1,0,0);
        if (MixMoves == 1)
        {
        glTranslatef(POSX8x3,POSY8x3,POSZ8x3);
        glRotatef(Angle8x3,RX8x3,RY8x3,RZ8x3);
        glTranslatef(POS2X8x3,POS2Y8x3,POS2Z8x3);
        glRotatef(90,-RX8x3,-RY8x3,-RZ8x3);
        }
        this->CubePiece8x3();
    glPopMatrix();
//                                                                                |
    glPushMatrix();// CUBE PIECE 9x3 (RED BLUE YELLOW)
        glTranslatef(2.2,2.2,2.2);
        glRotatef(90,1,0,0);
        if (MixMoves == 1)
        {
        glTranslatef(POSX9x3,POSY9x3,POSZ9x3);
        glRotatef(Angle9x3,RX9x3,RY9x3,RZ9x3);
        glTranslatef(POS2X9x3,POS2Y9x3,POS2Z9x3);
        glRotatef(90,-RX9x3,-RY9x3,-RZ9x3);
        }
        this->CubePiece9x3();
    glPopMatrix();
//                                                                                |
    glPopMatrix();// FIRST ROTATION
//                                                                                |
//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄SIDE 3┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘



//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄SLIDER ROTATION┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┐
    glPopMatrix();                     //                                         |
//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄SLIDER ROTATION┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘




//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄TESTING PIECE┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┐
    glPushMatrix();
    glScalef(4,4,4);
      glTranslatef(-3,3,0);
      glRotatef(7.1,0,1,0);
      glRotatef(7.1,-1,0,0);
      if(MixMoveD ==1)
      {
        glRotatef(90,0,1,0);
      }
        if (SPIN ==1)
        {
          glRotatef(90,0,1,0);
        }

        if (SPIN2 ==1)
        {
          glRotatef(90,0,1,0);
        }
        if(SPIN3 ==1)
        {
          glRotatef(90,0,0,-1);
        }

      this->TEST();
    glPopMatrix();
//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄TESTING PIECE┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘

*///---------------------------------------------------------------------------------METHOD 2----------------------------------------------------------------------------------




//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄AXIS┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┐
    materialStruct*p_front = &Emerald;

      glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
     glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
      glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
      glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
//                                                                                |
    glPushMatrix();   //X AXIS
      glTranslatef(0,0,-5.5);
      this->CylinderX();
    glPopMatrix();
//                                                                                |
    glPushMatrix();   //Y AXIS
      glTranslatef(-5.5,0,0);
      glRotatef(90,0,1,0);
      this->CylinderY();
    glPopMatrix();
//                                                                                |
    glPushMatrix();   //Z AXIS
      glTranslatef(0,5.5,0);
      glRotatef(90,1,0,0);
      this->CylinderZ();
    glPopMatrix();
//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄AXIS┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘


//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄PAINT GL┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┐
    glLoadIdentity(); //View point                                                |
    gluLookAt(1.,1,8., 0.0,0.0,0.0, 0.0,1.0,0.0); //                              |
    glFlush();//                                                                  |
  }                                               //                              |
//┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄PAINT GL┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘
