/****************************************************************************
** This is the main file where the basic computations are made
** This code is part of Individual Project Software
** Antreas Josephides ID :201026404
*****************************************************************************/
#include <QApplication>
#include <QVBoxLayout>
#include "RubicCubeWindow.h"
#include <GL/glut.h>

int main(int argc, char *argv[]) // main()
{

	QApplication app(argc, argv); // Creating the application with arguments as parameters


  RubicCubeWindow *window = new RubicCubeWindow(NULL); // create a master widget



	window->resize(1012, 1612); // Window size


	window->show(); 	// show the label


	app.exec(); 	// Execute application


	delete window; 	// clean up and delete controller;



	return 0; 	// return to caller

}
