THIS FILE IS PART OF THE INDIVIDUAL PROJECT AI FOR BOARD AND CARD GAMES OF 2018/2019 OF ANTREAS JOSEPHIDES ID:201026404
	

		THIS FILE THAT COINTAINS THE FOLLOWING:
	
				1) INFORMATION ON HOW TO COMPILE AND RUN THE GAME

				2) RULES OF THE GAME

		
		1) INFORMATION ON COMPILING AND RUNNING THE GAME


				A) UNZIP THE FILE
				
				B) NAVIGATE IN THE RUBICS CUBE GAME FOLDER
				
				C) RIGHT CLICK INSIDE THE FOLDER  

				D) CREATE TERMINAL

				E) TYPE "make" (and every time you want to compile the program)
		
				F) TYPE "./Rubic"

				G) THE GAME SHOULD START WORKING


		2) RULES OF THE GAME


				A) EACH PLAYER MUST HAVE A RUBIK'S CUBE ON HIS HANDS

				B) EACH PLAYER SHOULD HAVE ALREADY MIXED HIS CUBE BEFORE RUNNING THE GAME

				C) AFTER COMPILING AND RUNNING THE GAME, THE GAME CUBE WILL START MIXING

				D) THERE IS A SIGN ON THE TOP LEFT CORNER WHICH INFORMS THE PLAYERS ABOUT THE STATE OF THE GAME

				E) WHEN THE SIGN SAYS GO START SOLVING THE CUBE

				F) IF THE PLAYER SOLVES THE CUBE BEFORE THE AI PLAYER IT WINS OTHERWISE IT LOSES












